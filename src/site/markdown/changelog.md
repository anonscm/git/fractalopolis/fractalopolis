## Changelog

##### version 1.2.5 (15/01/2024)
- Add menu item Import fractal from another project
- Change color ramp for accessibilities
- Add housing_avg_empirical and diff attributes

##### version 1.2.4 (15/01/2024)
- Set Layer : can load Geopackage
- The code of a fractal element can be updated when rank IFS has changed
- Add IFS shapes : hexagon, pentagon, ...
- Accessibility v2 : types are updated if layer has changed
- Bug at project creation when housing attribute already exists

##### version 1.2.3 (26/04/2022)
- Update IO GIS library for better loading of shapefile
- Manage unrecognize CRS in project creation

##### version 1.2.2 (22/09/2021)
WARNING project is not compatible with previous version !
Replace java.util.Arrays$ArrayList with java.util.ArrayList in xml project
Remove listenerList and initShape in ifs elements

- Update for Java 16 compatibility
- Null pointer exception at project creation from 1.2.1 version

##### version 1.2.1 (10/09/2021)
- Add accessibility calculations on rural zones
- Link monitor view on rural layer

##### version 1.2 (23/07/2020)
- Add new accessibility rules set V2

##### version 1.1.5 (11/06/2020)
- Add Save as menu item
- Housing model window is no more modal
- Add density-model attribute to urban features
- Add density-empirical and density-model attributes to rural features
- Change ratio attribute calculation

##### version 1.1.4 (19/05/2020)
- Menu File -> SetLayer : add Other layer item to add additionnal layers
- Add diff housing and diff rate for rural features
- Optimize rendering when moving fractal square
- Rural transparency

##### version 1.1.3 (29/04/2019)
- Housing density : change unit from housing/km2 to housing/ha and takes into account restricted area

##### version 1.1.2 (02/03/2018)
- Null pointer exception while creating new project

##### version 1.1.1 (06/12/2017)
- Null pointer exception while creating Micro Scale

##### version 1.1 (21/11/2017)
- shape elements can be changed manually ("Edit shape" button)
- evaluations updates have been optimized and threaded
- optional layers can be removed by right clicking on it

##### version 1.0.1 (14/11/2017)
- add "Auto update" checkbox for disabling auto update
- add "Add layer" button

##### version 1.0 (21/04/2017)
WARNING project is not compatible with previous version !

- rename some elements
- refactor Housing model form

##### version 0.9.5 (13/03/2017)
- After project creation ifs shows 2 elements instead of just one

##### version 0.9.4 (11/03/2017)
- Project creation throw Null pointer exception
- Dwelling model window : change step from 1,2,3 to a,b,c
- Change rural attribute size to area

##### version 0.9.3 (18/01/2017)
WARNING project is not compatible with previous version !

- refactor Dwelling model form
- rename a lot of elements : green to leisure, pop to dwelling, ...
- rename and remove a lot of elements from object properties 

##### version 0.9.2 (29/11/2016)
- add "Maximum step" parameter to accessibility evaluations

##### version 0.9.1 (10/11/2016)
WARNING project is not compatible with previous version !

- rename a lot of elements : facility to shop, leisure to green, level to freq, ...
- move to back the build raster
- add "Remove" button for removing iterations
- add "Land potential" button
- remove "code" attribute from view combobox (throw exception)
- shape changes can be undone by Ctrl+Z

##### version 0.9 (05/09/2016)
- add restricted area evaluation

##### version 0.8.2 (09/03/2016)
- add userdens attribute = userpop / (area * 0.7) in ha

##### version 0.8.1 (23/09/2015)
- bug : initial population for user model cannot be set
- remove rural density
- add new rural calculation for real and user model
- add rural layer

##### version 0.8 (08/04/2015)
WARNING project is not compatible with previous version !

- update thema libs
- cleaning code

##### version 0.7.1 (14/02/2014)
- expand root node of tree layer (bug on windows)

##### version 0.7 (04/01/2014)
WARNING Project is not compatible : replace fractalopolis. with org.thema.fractalopolis. in xml project file

- move project to maven and dev server
- shapefile export : export also rural areas

##### version 0.6.1 (21/08/2013)
- add transparency to evaluation view
- compute d0 button in IFS editor
