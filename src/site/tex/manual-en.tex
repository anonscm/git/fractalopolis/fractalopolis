\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amssymb,amsfonts,textcomp}
\usepackage{array}
\usepackage{hhline}
\usepackage{hyperref}
\hypersetup{colorlinks=true, linkcolor=blue, citecolor=blue, filecolor=blue, urlcolor=blue, pdftitle=, pdfauthor=Gilles Vuidel, pdfsubject=, pdfkeywords=}
\usepackage{graphicx}
\usepackage[top=2.501cm,bottom=2.501cm,left=2.501cm,right=2.501cm,nohead]{geometry}
\usepackage{float}
\usepackage{parskip}
\usepackage{caption}
\makeatletter
\newcommand\arraybslash{\let\\\@arraycr}
\makeatother
% centering figures
\makeatletter
\g@addto@macro\@floatboxreset\centering
\makeatother
\setlength\tabcolsep{1mm}
\renewcommand\arraystretch{1.3}

% saut de page après une section
%\let\oldsection\section
%\renewcommand\section{\clearpage\oldsection}

% saut après itemize
\let\EndItemize\enditemize
\def\enditemize{\EndItemize\medskip}

\begin{document}

\begin{titlepage}

	\centering
	\bigskip
	\bigskip	
	{\Huge
	\bfseries
	Fractalopolis 1.1.5\\
	\bigskip
	User guide\\
	}
	\bigskip
	\bigskip
	\bigskip
	\bigskip
	\bigskip
			
	{\Large		
			June 2020\\
	}
	
\end{titlepage}

\setcounter{tocdepth}{2}
\tableofcontents

\pagebreak

\section{Introduction}

\subsection{About Fractalopolis}

Fractalopolis is a software application for creating local or regional multifractal development plans. It can also be used for evaluating the suitability of each urban centre for future development. Two types of zones are distinguished in Fractalopolis, those opened for urbanization (planned urban centres) and others for which only restricted development or no development at all is authorized (areas located outside the planned centres).

The plan begins with a cartographic representation of the region under study: buildings, shops and services, leisure facilities and green areas, non-developable areas, and transport networks. The plan is then created following an iterative logic based on the use of Iterative Function Systems (Barnsley 1988). The initiator of the plan is a quadratic area of size $L$, which is placed over the region under study. Then a square of a chosen size $S_1 = r_1 \times L$ is centred on the existing main urban centre and $N$ squares of size $S_0 = r_0 \times L$ are centred on second-order urban centres that are to be developed. This defines the plan’s generator. Further iteration steps can be used to create additional sub-centres, which have to be sited appropriately according to the catchment area of higher-level urban centres, public transport stations, and non-developable areas. According to the fractal logic, those sub-centres cannot overlap one another nor can they overlap areas outside the centres created at previous iteration steps (i.e. mainly green and rural areas).

\begin{figure}[H]
	\includegraphics[scale=0.5]{img/main_window.png}
\end{figure}


\subsubsection{Terms of use}

Fractalopolis is distributed in open source, under the GPL licence. Users must cite the following reference in their publications: \\
Frankhauser P., Tannier C., Vuidel G., Houot H. (2018), An integrated multifractal modelling to urban and regional planning, Computers, Environment and Urban Systems, 67, 132–146. 


\subsection{System requirements}

Fractalopolis runs on any computer supporting Java 8 or later (PC under Linux, Windows, Mac, etc.). When dealing with very large datasets, the computer’s RAM memory can limit the running time. Processing power (CPU) determines computing speed. For details, see the section on \textit{Processing capabilities and limitations} below.

\subsection{Installing the software and launching a project}

Fractalopolis can be downloaded from \url{https://sourcesup.renater/fractalopolis/}.

\begin{itemize}
	\item Download and install Java 8 or later - java.com. If you have a 64-bit operating system, it is best to install the 64-bit version of Java.
	\item Download Fractalopolis-1.1.jar
	\item Launch Fractalopolis-1.1.jar
\end{itemize}

\section{Starting a project}

New projects are created from the \textit{File / New project} menu. 

The user must enter a project name and specify the folder in which it is to be created. Two shapefiles are required: buildings and number of housing units in each local community.

\begin{figure}[H]
	\includegraphics[scale=0.5]{img/new_project.png}
\end{figure}


\section{Setting information layers}

Besides the two shapefiles involved in the creation of a project, additional information layers can be downloaded from the \textit{File / Set layer} menu.

\begin{figure}[H]
	\includegraphics[scale=0.5]{img/set_layer.png}
\end{figure}

\textit{Shops} (content: shops and services represented by points) and \textit{Leisures} (content: green areas and leisure facilities also represented by points) layers are used to calculate the suitability of each centre for future development. These shapefiles must contained two fields: TYPE (i.e. the type of facility to which each point corresponds) and FREQ (i.e. how often each point is used – FREQ=1, rarely; FREQ=2, monthly; FREQ=3, weekly; FREQ=4, daily).

Other layers are used as guides to site the planned centres of a multifractal plan appropriately (e.g. non-developable areas, public transport stations, etc.).


\section{Macro scale IFS}
The IFS editor is accessible from the \textit{Macro scale / Set the IFS} menu.

\begin{figure}[H]
	\includegraphics[scale=0.5]{img/ifs_editor.png}
\end{figure}

When setting an IFS, remember to specify the square that is the main centre (rank 1) and the squares that are the sub-centres (rank 0).

\section{Designing a multifractal development plan}

\subsection{Initiator}
Set the position and the size of the initiator.

\begin{figure}[H]
	\includegraphics[scale=0.5]{img/initiator.png}
\end{figure}

\subsection{Other iteration steps}
Open the \textit{Macro scale monitor} from the \textit{Macro scale / monitor} menu.

Apply the IFS until the chosen iteration step (usually two or three steps) by clicking on the \textit{Add} button.

\begin{figure}[H]
	\includegraphics[scale=0.4]{img/apply_ifs.png}
\end{figure}

Use the arrow in the main window to position each planned urban centre appropriately.

The properties of a centre can be displayed by clicking on the \textit{Properties} button in the main window and then clicking on the targeted centre.

\begin{figure}[H]
	\includegraphics[scale=0.4]{img/properties.png}
\end{figure}

\begin{itemize}
	\item \textsl{code}: numerical code attributed to each centre according to its hierarchical level. Each digit of the code corresponds to an iteration step. From left to right: first iteration step, second iteration step, and third iteration step. The reduction factor $r_1$ corresponds to each 1 digit and $r_0$ to each 0 digit.
	\item \textsl{level}: hierarchical level of each centre. A supplementary (lower) hierarchical level appears at each iteration step. The generator contains two hierarchical levels; the second iteration step adds a third hierarchical level; the third iteration step adds a fourth hierarchical level.
	\item \textsl{size}: width in metres
	\item \textsl{built-area}: built-up area in square metres
	\item \textsl{building-land-potential}: potential building land in each planned urban centre. The corresponding evaluation rule is: building land potential equals 0 when non-developable areas represent 50\% or more of the area of the centre under consideration; building land potential equals 1 when non-developable areas represent 0\% of the area of the centre; linear increase in the building land potential between 0 and 1 when non-developable areas represents from 0 to 50\% of the area of the centre.
	\item \textsl{housing-empirical}: current number of housing units in the centre
	\item \textsl{housing-density-empirical}: number of housing units per hectare 
	\item \textsl{housing-model}: number of housing units calculated by the housing model
	\item \textsl{housing-density-model}: number of housing units calculated by the housing model per hectare 
	\item \textsl{diff-model-empirical}: absolute difference between the current number of housing units and the number of housing units calculated by the housing model
	\item \textsl{rate-model-empirical}: \textit{(housing-model} $-$ \textit{housing-empirical)} $/$ \textit{housing-empirical}
	\item \textsl{shop1}, \textsl{shop2}, \textsl{shop3}, \textsl{shop4}: accessibility to shops and services of respectively rarely, monthly, weekly, and daily frequency of recourse; values between 0 and 1.
	\item \textsl{leisure1}, \textsl{leisure2}, \textsl{leisure3}, \textsl{leisure4}: accessibility to leisure infrastructures and green areas used rarely, monthly, weekly, and daily, respectively; values between 0 and 1.
	\item \textsl{suitability}: suitability of each centre planned for future development; value between 0 and 1.
\end{itemize}

\subsection{Assess the suitability of each planned centre}

From the \textit{Macro scale / Assess the suitability of each planned centre} menu, set the parameters used to assess the suitability of each centre for development (evaluation rules and weights involved in the aggregations).

\begin{figure}[H]
	\includegraphics[scale=0.3]{img/param_suitability.png}
\end{figure}

Display assessment results from the \textit{Macro scale monitor / View: suitability}.

\begin{figure}[H]
	\includegraphics[scale=0.4]{img/view_suitability.png}
\end{figure}

\subsection{Housing model}

The housing model enables the spatial distribution of the housing units that will be constructed in future. It is accessible from the \textit{Macro scale monitor} by clicking on the \textit{Housing model} button.

\begin{figure}[H]
	\includegraphics[scale=0.4]{img/housing_model.png}
\end{figure}

The whole urban region contains a given number of housing units $\nu$ ($\nu=115 500$ in the example above). Some of them are located outside the centres marked for development. The very rural areas contain $(1-\alpha)\nu$ housing units. The rural areas that appear at the second iteration step contain $\alpha(1-\beta)\nu$ housing units. By the same logic, rural areas that appear at the third iteration step contain $\alpha \beta (1-\gamma)\nu$ housing units. The remaining housing units $\alpha \gamma \beta(\nu)$ is distributed among the urban centres according to their hierarchical code. Weighting factors are introduced to do this. Factor $a$ corresponds to the first digit of the code, factor $b$  to the second digit, and factor $c$ to the third digit. The subscript 1 of each weighting factor corresponds to the code digit 1 and conversely, the subscript 0 corresponds to the code digit 0.

The choice of weighting-factor values enables a spatial distribution of the housing units following a defined planning strategy. If $a_1 > b_1 > c_1$ more housing units are concentrated in urban centres for which the first code digit is 1. Conversely, a higher $b_1$ value reinforces the weight of second-order central places 011 and 010 and, at the same time, centres 111 and 110 remain populated.


\begin{itemize}
	\item \textsl{Empirical parameters} table: values of parameters $a$, $b$ and $c$ for the current (initial) situation.
	\item \textsl{Model parameters} table: values of parameters $a$, $b$ and $c$ for the housing model.
	\item \textsl{Number of housing units} table:
	\begin{itemize}
		\item \textsl{Nb}: number of centres with the corresponding code;
		\item \textsl{Total emp}: total number of housing units in the initial situation for all centres with the relevant code;
		\item \textsl{Avg emp}: average number of housing units in the initial situation for each centre with the relevant code;
		\item \textsl{Min emp}: minimum number of housing units in the initial situation;
		\item \textsl{Max emp}: maximum number of housing units in the initial situation;
		\item \textsl{Total model}: number of housing units given by the model for all centres with the relevant code;
		\item \textsl{Avg model}: average number of housing units given by the model in each centre with the relevant code.
	\end{itemize}
\end{itemize}

\section{Export of maps (shapefiles) and images (views)}

Right-click on the name of a layer to export it in a shapefile format.

\begin{figure}[H]
	\includegraphics[scale=0.4]{img/export_shapefile.png}
\end{figure}

The \textit{Export view} button in the main window can be used to export a view in \textit{svg} or \textit{png} format.

\section{Processing capabilities and limitations}

The memory used by the software plays an important role. Computing will be slow or may fail if there is not enough RAM (OutOfMemoryError or GC Overhead message). The \textit{File/ Preferences / Memory} menu can be used to adjust the memory allocated to Fractalopolis. If you have a 32-bit version of Java, Fractalopolis will be limited to about 2 Go (2000 Mo) of memory. If your computer has more than 2 Go of RAM memory, it is recommended to install the 64-bit version of Java in order to extend the available memory beyond 2 Go.


\end{document}
