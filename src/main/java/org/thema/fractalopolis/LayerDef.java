/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.fractalopolis;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.thema.drawshape.style.FeatureStyle;

/**
 *
 * @author gvuidel
 */
public class LayerDef {
    private String name;
    private String desc;
    private FeatureStyle style;
    private List<String> attrNames;
    private List<Class> attrClasses;

    public LayerDef(String name, String desc, FeatureStyle style) {
        this.name = name;
        this.desc = desc;
        this.style = style;
        this.attrNames = Collections.EMPTY_LIST;
    }
    
    public LayerDef(String name, String desc, FeatureStyle style, String attr, Class type) {
        this(name, desc, style);
        this.attrNames = Arrays.asList(attr);
        this.attrClasses = Arrays.asList(type);
    }
    
    public LayerDef(String name, String desc, FeatureStyle style, String attr1, Class type1, String attr2, Class type2) {
        this(name, desc, style);
        this.attrNames = Arrays.asList(attr1, attr2);
        this.attrClasses = Arrays.asList(type1, type2);
    }

    @Override
    public String toString() {
        return desc;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public FeatureStyle getStyle() {
        return style;
    }

    public List<String> getAttrNames() {
        return attrNames;
    }

    public List<Class> getAttrClasses() {
        return attrClasses;
    }
    
    
    
}
