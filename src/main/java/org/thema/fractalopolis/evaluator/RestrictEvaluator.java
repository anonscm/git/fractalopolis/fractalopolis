/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis.evaluator;

import org.locationtech.jts.geom.Geometry;
import java.util.List;
import org.thema.data.feature.DefaultFeature;
import org.thema.fractalopolis.Project;
import org.thema.fractalopolis.ifs.FractalElem;

/**
 *
 * @author gvuidel
 */
public class RestrictEvaluator implements Evaluator {

    private Project project;
    
    public RestrictEvaluator(Project project) {
        this.project = project;
    }
    
    @Override
    public void updateEval(FractalElem elem) {
        Geometry geom = elem.getGeometry();
        List<DefaultFeature> restrictZones = project.getCoverage(Project.Layers.RESTRICT.toString()).getFeatures(geom.getEnvelopeInternal());
        double area = 0;
        for(DefaultFeature zone : restrictZones) {
            double a;
            if(geom.getEnvelopeInternal().contains(zone.getGeometry().getEnvelopeInternal())) {
                a = zone.getGeometry().getArea();
            } else {
                a = zone.getGeometry().intersection(geom).getArea();
            }
            area += a;
        }
        
        elem.setRestrictArea(area);
    }

    @Override
    public boolean isEnabled() {
        return project.isLayerExist(Project.Layers.RESTRICT.toString());
    }

    @Override
    public String toString() {
        return "Restricted areas";
    }
    
    
}
