/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis.evaluator;

import org.locationtech.jts.geom.Geometry;
import java.util.List;
import org.thema.data.feature.DefaultFeature;
import org.thema.fractalopolis.Project;
import org.thema.fractalopolis.ifs.FractalElem;

/**
 *
 * @author gvuidel
 */
public class BuildPopEvaluator implements Evaluator {

    private Project project;
    private String buildLayer;
    
    public BuildPopEvaluator(Project project, String buildLayer) {
        this.project = project;
        this.buildLayer = buildLayer;
    }
    
    @Override
    public void updateEval(FractalElem elem) {
        Geometry geom = elem.getGeometry();
        List<DefaultFeature> builds = project.getCoverage(buildLayer).getFeatures(geom.getEnvelopeInternal());
        double area = 0, pop = 0;
        for(DefaultFeature build : builds) {
            double a;
            if(geom.getEnvelopeInternal().contains(build.getGeometry().getEnvelopeInternal())) {
                a = build.getGeometry().getArea();
            } else {
                a = build.getGeometry().intersection(geom).getArea();
            }
            pop += ((Number)build.getAttribute(Project.HOUSING_ATTR)).doubleValue() * a / build.getGeometry().getArea();
            area += a;
        }
        
        elem.setBuildArea(area);
        elem.setHousing(pop);
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String toString() {
        return "Housings";
    }
    
}
