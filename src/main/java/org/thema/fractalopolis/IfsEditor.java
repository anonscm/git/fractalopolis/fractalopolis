/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.fractalopolis;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.solvers.BisectionSolver;
import org.thema.common.Util;
import org.thema.drawshape.GridShape;
import org.thema.drawshape.PanelMap;
import org.thema.drawshape.SelectableShape;
import org.thema.drawshape.layer.DefaultGroupLayer;
import org.thema.drawshape.layer.DefaultLayer;
import org.thema.drawshape.style.SimpleStyle;
import org.thema.fractalopolis.ifs.Ifs;

/**
 *
 * @author  gvuidel
 */
public class IfsEditor extends javax.swing.JFrame {
    
    private Ifs ifs;
    private RasterFractalFrame previewFrame;
    
    /** Creates new form IfsEditor */
    public IfsEditor(Ifs i) {
       
        if(i == null) {
            ifs = new Ifs();
        } else {
            ifs = i;
        }
       
        initComponents();

        final PanelMap map = viewer.getMap();
        table.setModel(ifs.getTableModel());
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int ind = table.getSelectedRow();
                if(ind >= 0) {
                    map.setSelection((List)Arrays.asList(ifs.getLayer().getDrawableShapes().get(ind)));
                }
            }
        });
        map.addShapeSelectionListener(new PanelMap.ShapeSelectionListener() {
            @Override
            public void selectionChanged(PanelMap.ShapeSelectionEvent event) {
                table.clearSelection();
                Collection<SelectableShape> selShapes = event.getShapesAdded();
                if(selShapes.isEmpty()) {
                    return;
                }
                int ind = ifs.getLayer().getDrawableShapes().indexOf(selShapes.iterator().next());
                
                if(ind != -1) {
                    table.addRowSelectionInterval(ind, ind);
                }
            }
        });

        DefaultGroupLayer gl = new DefaultGroupLayer("root", true);
        gl.addLayerFirst(ifs.getLayer());
        
        GridShape shape = new GridShape(new Rectangle2D.Double(0, 0, 1, 1), 0.1);
        shape.setStyle(new SimpleStyle(Color.LIGHT_GRAY));
        gl.addLayerLast(new DefaultLayer("Grid", shape));
        viewer.setRootLayer(gl);

        previewFrame = new RasterFractalFrame(ifs);
        previewFrame.setLocationRelativeTo(this);
        overlapToggleButton.doClick();
        limitToggleButton.doClick();

    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        toolbarPanel = new javax.swing.JPanel();
        frameToolBar = new javax.swing.JToolBar();
        jSeparator2 = new javax.swing.JToolBar.Separator();
        previewToggleButton = new javax.swing.JToggleButton();
        closeButton = new javax.swing.JButton();
        constraintToolBar = new javax.swing.JToolBar();
        initShapeButton = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        limitToggleButton = new javax.swing.JToggleButton();
        overlapToggleButton = new javax.swing.JToggleButton();
        jSeparator3 = new javax.swing.JToolBar.Separator();
        d0Button = new javax.swing.JButton();
        splitPane = new javax.swing.JSplitPane();
        tabIfsPanel = new javax.swing.JPanel();
        addButton = new javax.swing.JButton();
        scrollPaneTable = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        removeButton = new javax.swing.JButton();
        loadButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        dupButton = new javax.swing.JButton();
        gridButton = new javax.swing.JButton();
        viewer = new org.thema.drawshape.ui.MapViewer();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Ifs Editor");
        setLocationByPlatform(true);

        frameToolBar.add(jSeparator2);

        previewToggleButton.setText("Preview");
        previewToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                previewToggleButtonActionPerformed(evt);
            }
        });
        frameToolBar.add(previewToggleButton);

        closeButton.setText("Close");
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });
        frameToolBar.add(closeButton);

        initShapeButton.setText("Initiator shape");
        initShapeButton.setFocusable(false);
        initShapeButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        initShapeButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        initShapeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                initShapeButtonActionPerformed(evt);
            }
        });
        constraintToolBar.add(initShapeButton);
        constraintToolBar.add(jSeparator1);

        limitToggleButton.setAction(limitAction);
        limitToggleButton.setText("Limit");
        constraintToolBar.add(limitToggleButton);

        overlapToggleButton.setAction(overlapAction);
        overlapToggleButton.setText("Overlap");
        constraintToolBar.add(overlapToggleButton);
        constraintToolBar.add(jSeparator3);

        d0Button.setText("Compute D0");
        d0Button.setFocusable(false);
        d0Button.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        d0Button.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        d0Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                d0ButtonActionPerformed(evt);
            }
        });
        constraintToolBar.add(d0Button);

        org.jdesktop.layout.GroupLayout toolbarPanelLayout = new org.jdesktop.layout.GroupLayout(toolbarPanel);
        toolbarPanel.setLayout(toolbarPanelLayout);
        toolbarPanelLayout.setHorizontalGroup(
            toolbarPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(toolbarPanelLayout.createSequentialGroup()
                .add(constraintToolBar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 287, Short.MAX_VALUE)
                .add(frameToolBar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );
        toolbarPanelLayout.setVerticalGroup(
            toolbarPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, toolbarPanelLayout.createSequentialGroup()
                .add(toolbarPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, frameToolBar, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(constraintToolBar, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        splitPane.setResizeWeight(0.3);

        addButton.setText("Add");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scrollPaneTable.setViewportView(table);

        removeButton.setText("Remove");
        removeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeButtonActionPerformed(evt);
            }
        });

        loadButton.setText("Load...");
        loadButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadButtonActionPerformed(evt);
            }
        });

        saveButton.setText("Save...");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });

        dupButton.setText("Dup");
        dupButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dupButtonActionPerformed(evt);
            }
        });

        gridButton.setText("Grid");
        gridButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gridButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout tabIfsPanelLayout = new org.jdesktop.layout.GroupLayout(tabIfsPanel);
        tabIfsPanel.setLayout(tabIfsPanelLayout);
        tabIfsPanelLayout.setHorizontalGroup(
            tabIfsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(tabIfsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(addButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(removeButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(dupButton)
                .add(43, 43, 43)
                .add(loadButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(saveButton)
                .add(18, 18, 18)
                .add(gridButton)
                .addContainerGap(33, Short.MAX_VALUE))
            .add(scrollPaneTable, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 417, Short.MAX_VALUE)
        );
        tabIfsPanelLayout.setVerticalGroup(
            tabIfsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, tabIfsPanelLayout.createSequentialGroup()
                .add(scrollPaneTable, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 351, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(tabIfsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(addButton)
                    .add(removeButton)
                    .add(dupButton)
                    .add(saveButton)
                    .add(loadButton)
                    .add(gridButton))
                .addContainerGap())
        );

        splitPane.setRightComponent(tabIfsPanel);
        splitPane.setLeftComponent(viewer);

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(toolbarPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(splitPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 768, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(toolbarPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 37, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(splitPane))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void previewToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_previewToggleButtonActionPerformed
        previewFrame.setVisible(previewToggleButton.isSelected());
    }//GEN-LAST:event_previewToggleButtonActionPerformed

    private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeButtonActionPerformed
        previewFrame.dispose();
        dispose();
    }//GEN-LAST:event_closeButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        File f = Util.getFileSave(".txt");
        if(f == null) {
            return;
        }
        
        try {
            ifs.exportFile(f.getAbsolutePath());
        } catch(IOException e) {
            JOptionPane.showMessageDialog(this, "Error while saving ifs.\n"+e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_saveButtonActionPerformed

    private void loadButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadButtonActionPerformed
        File f = Util.getFile(".txt", "Text file");
        if(f == null) {
            return;
        }

        try {
            ifs.importFile(f.getAbsolutePath());
        } catch(Exception e) {
            JOptionPane.showMessageDialog(this, "Error while reading matrix file.\n"+e, "Error", JOptionPane.ERROR_MESSAGE);
        }
                    
    }//GEN-LAST:event_loadButtonActionPerformed

    private void removeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeButtonActionPerformed
        if(table.getSelectedRow() > -1) {
            ifs.removeTransform(table.getSelectedRow());
        }
    }//GEN-LAST:event_removeButtonActionPerformed

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        ifs.addTransform(new AffineTransform());
    }//GEN-LAST:event_addButtonActionPerformed

    private void dupButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dupButtonActionPerformed
        if(table.getSelectedRow() < 0) {
            return;
        }
        ifs.addTransform(new AffineTransform(ifs.getTransform(table.getSelectedRow())));
    }//GEN-LAST:event_dupButtonActionPerformed

    private void gridButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_gridButtonActionPerformed
        String res = JOptionPane.showInputDialog("Grid size", 3);
        if(res == null) {
            return;
        }

        double n = Integer.parseInt(res);
        for(int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) { 
                ifs.addTransform(new AffineTransform(1/n, 0, 0, 1/n, i/n, j/n));
            }
        }
            
    }//GEN-LAST:event_gridButtonActionPerformed

    private void initShapeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_initShapeButtonActionPerformed
        String [] shapes = new String[]{"Square", "Circle", "Hexagon", "Pentagon", "Triangle"};

        int res = JOptionPane.showOptionDialog(this, "Select initator shape", "Shape",
                 JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, shapes, shapes[0]);

        if(res == JOptionPane.CLOSED_OPTION) {
            return;
        }

        ifs.setInitShape(res);
    }//GEN-LAST:event_initShapeButtonActionPerformed

    private void d0ButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_d0ButtonActionPerformed
        double d0 = new BisectionSolver().solve(1000, new UnivariateFunction() {
            @Override
            public double value(double d) {
                double sum = -1;
                for(int i = 0; i < ifs.getNbTransform(); i++) {
                    AffineTransform t = ifs.getTransform(i);
                    sum += Math.pow(t.getScaleX(), d);
                }
                return sum;
            }
        }, 0, 2);
        JOptionPane.showMessageDialog(this, "D0 : " + d0);
    }//GEN-LAST:event_d0ButtonActionPerformed
    
    private final AbstractAction overlapAction = new AbstractAction("Limit") {
        @Override
	public void actionPerformed(ActionEvent e) {
	    ifs.getLayer().setVerifyOverlap(overlapToggleButton.isSelected());
	}
    };
    private final AbstractAction limitAction = new AbstractAction("Overlap") {
        @Override
	public void actionPerformed(ActionEvent e) {
	    ifs.getLayer().setVerifyParentLimit(limitToggleButton.isSelected());
	}
    };
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JButton closeButton;
    private javax.swing.JToolBar constraintToolBar;
    private javax.swing.JButton d0Button;
    private javax.swing.JButton dupButton;
    private javax.swing.JToolBar frameToolBar;
    private javax.swing.JButton gridButton;
    private javax.swing.JButton initShapeButton;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JToolBar.Separator jSeparator2;
    private javax.swing.JToolBar.Separator jSeparator3;
    private javax.swing.JToggleButton limitToggleButton;
    private javax.swing.JButton loadButton;
    private javax.swing.JToggleButton overlapToggleButton;
    private javax.swing.JToggleButton previewToggleButton;
    private javax.swing.JButton removeButton;
    private javax.swing.JButton saveButton;
    private javax.swing.JScrollPane scrollPaneTable;
    private javax.swing.JSplitPane splitPane;
    private javax.swing.JPanel tabIfsPanel;
    private javax.swing.JTable table;
    private javax.swing.JPanel toolbarPanel;
    private org.thema.drawshape.ui.MapViewer viewer;
    // End of variables declaration//GEN-END:variables
    
}
