/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis;

import java.awt.Cursor;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import org.thema.drawshape.ui.MapViewer;
import org.thema.fractalopolis.ifs.Fractal;
import org.thema.fractalopolis.ifs.Fractal.Scale;
import org.thema.fractalopolis.ifs.FractalElem;

/**
 *
 * @author gvuidel
 */
public final class StatInfoDialog extends javax.swing.JDialog {

    private Fractal fractal;
    private MapViewer mapViewer;
    
    /**
     * Creates new form StatInfoDialog
     */
    public StatInfoDialog(java.awt.Frame parent, MapViewer mapViewer, Fractal fractal) {
        super(parent, false);
        this.fractal = fractal;
        this.mapViewer = mapViewer;
        initComponents();
        
        if(fractal.getScale() == Scale.MICRO) {
            setTitle("Micro scale monitor");
        } else {
            setTitle("Macro scale monitor");
        }
        
        DefaultComboBoxModel model = new DefaultComboBoxModel(fractal.getFractalModel().getRootElem().getAttributeNames().toArray());
        model.removeElement("code");
        model.insertElementAt("(transparent)", 0);
        model.setSelectedItem("(transparent)");
        viewComboBox.setModel(model);
        refreshStat();
        autoUpdateCheckBox.setSelected(fractal.isAutoUpdate());
        fractal.setProgressBar(progressBar);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        statTextArea = new javax.swing.JTextArea();
        updateButton = new javax.swing.JButton();
        genToolBar = new javax.swing.JToolBar();
        previousStepButton = new javax.swing.JButton();
        nextStepButton = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        reinitButton = new javax.swing.JButton();
        addButton = new javax.swing.JButton();
        remButton = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JToolBar.Separator();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        modelPopButton = new javax.swing.JButton();
        jToolBar1 = new javax.swing.JToolBar();
        jLabel1 = new javax.swing.JLabel();
        viewComboBox = new javax.swing.JComboBox();
        landPotToggleButton = new javax.swing.JToggleButton();
        autoUpdateCheckBox = new javax.swing.JCheckBox();
        progressBar = new org.thema.common.swing.ProgressBarPanel();

        setTitle("Large scale stats");
        setLocationByPlatform(true);

        statTextArea.setColumns(20);
        statTextArea.setRows(5);
        jScrollPane1.setViewportView(statTextArea);

        updateButton.setText("Update stats");
        updateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateButtonActionPerformed(evt);
            }
        });

        previousStepButton.setAction(previousStepAction);
        genToolBar.add(previousStepButton);

        nextStepButton.setAction(nextStepAction);
        genToolBar.add(nextStepButton);
        genToolBar.add(jSeparator1);

        reinitButton.setAction(reInitAction);
        genToolBar.add(reinitButton);

        addButton.setAction(addLevelAction);
        addButton.setFocusable(false);
        addButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        addButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        genToolBar.add(addButton);

        remButton.setText("Remove");
        remButton.setFocusable(false);
        remButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        remButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        remButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                remButtonActionPerformed(evt);
            }
        });
        genToolBar.add(remButton);
        genToolBar.add(jSeparator2);
        genToolBar.add(filler1);

        modelPopButton.setText("Housing model");
        modelPopButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modelPopButtonActionPerformed(evt);
            }
        });
        genToolBar.add(modelPopButton);

        jToolBar1.setRollover(true);

        jLabel1.setText("View ");
        jToolBar1.add(jLabel1);

        viewComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewComboBoxActionPerformed(evt);
            }
        });
        jToolBar1.add(viewComboBox);

        landPotToggleButton.setText("Land potential");
        landPotToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                landPotToggleButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(landPotToggleButton);

        autoUpdateCheckBox.setText("Auto update");
        autoUpdateCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                autoUpdateCheckBoxActionPerformed(evt);
            }
        });

        progressBar.setPreferredSize(new java.awt.Dimension(200, 22));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(genToolBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(updateButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(autoUpdateCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(progressBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(6, 6, 6))
            .addComponent(jScrollPane1)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(genToolBar, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 265, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(updateButton)
                        .addComponent(autoUpdateCheckBox))
                    .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void updateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateButtonActionPerformed
      
    new Thread(new Runnable() {
        @Override
        public void run() {
            StatInfoDialog.this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            fractal.forceUpdate();
            refreshStat();
            StatInfoDialog.this.setCursor(Cursor.getDefaultCursor());
            mapViewer.getMap().repaintSelection();
        }
    }).start();
      
    }//GEN-LAST:event_updateButtonActionPerformed

    private void viewComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewComboBoxActionPerformed
        if(viewComboBox.getSelectedIndex() == 0) {
            fractal.getFractalGroupLayer().setView(null, landPotToggleButton.isSelected());
        } else { 
            fractal.getFractalGroupLayer().setView(viewComboBox.getSelectedItem().toString(), landPotToggleButton.isSelected());
        }
    }//GEN-LAST:event_viewComboBoxActionPerformed

    private void modelPopButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modelPopButtonActionPerformed
        if(fractal.getFractalModel().getNbIteration() == 0) {
            JOptionPane.showMessageDialog(this, "You must add one step before.", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        HousingModelDialog dlg = new HousingModelDialog((Frame)this.getParent(), fractal);
        dlg.setVisible(true);

        fractal.updateUserStat(fractal.getFractalModel().getRootElem());
    }//GEN-LAST:event_modelPopButtonActionPerformed

    private void landPotToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_landPotToggleButtonActionPerformed
        viewComboBoxActionPerformed(evt);
    }//GEN-LAST:event_landPotToggleButtonActionPerformed

    private void remButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_remButtonActionPerformed
        int res = JOptionPane.showConfirmDialog(this, "Iteration " + getStep() +
                " and the following will be removed.\nDo you want to remove it ?", "Remove iteration", JOptionPane.YES_NO_OPTION);
        if(res == JOptionPane.NO_OPTION) {
            return;
        }
        fractal.getFractalModel().removeIteration(getStep());
        previousStepButton.doClick();
    }//GEN-LAST:event_remButtonActionPerformed

    private void autoUpdateCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_autoUpdateCheckBoxActionPerformed
        fractal.setAutoUpdate(autoUpdateCheckBox.isSelected());
    }//GEN-LAST:event_autoUpdateCheckBoxActionPerformed

    
    private int getStep() {
        return fractal.getFractalGroupLayer().getIteration();
    }
    
    public void refreshStat() {
        
        for(String attr : fractal.getFractalModel().getRootElem().getAttributeNames()) {
            if (((DefaultComboBoxModel)viewComboBox.getModel()).getIndexOf(attr) < 0 && attr.toLowerCase().startsWith("m")) {
                ((DefaultComboBoxModel)viewComboBox.getModel()).addElement(attr);
            }
        }
        
        double pop = 0, build = 0, user = 0;
        List<FractalElem> elems = fractal.getFractalModel().getIteration(getStep());
        for(FractalElem  elem : elems) {
            pop += elem.getHousing();
            build += elem.getBuildArea();
            user += elem.getUser();
        }
        double popP = 0, buildP = 0, userP = 0;
        if(getStep() > 0) {
            elems = fractal.getFractalModel().getIteration(getStep()-1);
            for(FractalElem  elem : elems) {
                popP += elem.getHousing();
                buildP += elem.getBuildArea();
                userP += elem.getUser();
            }
        } else {
            popP = pop; 
            buildP = build; 
            userP = user;
        }
        FractalElem root = fractal.getFractalModel().getRootElem();
        
        String stats = String.format("Step : %d\n\tStep %d\tStep %d\tStep 0\n"
                + "Built area:\t%g\t%.1f%%\t%.1f%%\n"
                + "Housings\n"
                + "Empirical:\t%g\t%.1f%%\t%.1f%%\n"
                + "Model:\t%g\t%.1f%%\t%.1f%%", 
                getStep(), getStep(), getStep()-1,
                build, 100*build/buildP, 100*build/root.getBuildArea(),
                pop, 100*pop/popP, 100*pop/root.getHousing(), 
                user, 100*user/userP, 100*user/root.getUser());
            
        statTextArea.setText(stats);
    }
   
    private AbstractAction nextStepAction = new AbstractAction("Next", null) {
        @Override
	public void actionPerformed(ActionEvent e) {
	    if(fractal.getFractalModel().getNbIteration() == fractal.getFractalGroupLayer().getIteration()) {
                return;
            }
            mapViewer.getMap().clearSelection();
            fractal.getFractalGroupLayer().setIteration(fractal.getFractalGroupLayer().getIteration()+1);
            refreshStat();
	}
    };
    private AbstractAction previousStepAction = new AbstractAction("Previous", null) {
        @Override
	public void actionPerformed(ActionEvent e) {
            if(fractal.getFractalGroupLayer().getIteration() == 0) {
                return;
            }
	    mapViewer.getMap().clearSelection();
            fractal.getFractalGroupLayer().setIteration(fractal.getFractalGroupLayer().getIteration()-1);
            refreshStat();
	}
    };
    private AbstractAction reInitAction = new AbstractAction("Init", null) {
        @Override
	public void actionPerformed(ActionEvent e) {
            int res = JOptionPane.showConfirmDialog(StatInfoDialog.this, "This action will remove all fractal steps.\n Do you want to continue ?", "", JOptionPane.YES_NO_OPTION);
            if(res != JOptionPane.YES_OPTION) {
                return;
            }
	    mapViewer.getMap().clearSelection();
            fractal.reset();
            updateButtonActionPerformed(null);
	}
    };
    private AbstractAction addLevelAction = new AbstractAction("Add", null) {
        @Override
	public void actionPerformed(ActionEvent e) {
            if(fractal.getFractalModel().getNbIteration() == 4) {
                JOptionPane.showMessageDialog(null, "Cannot add more than 4 steps");
                return;
            }
            mapViewer.getMap().clearSelection();
            fractal.generate(1);
            fractal.getFractalGroupLayer().setIteration(fractal.getFractalModel().getNbIteration());
            updateButtonActionPerformed(null);
	}
    };
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JCheckBox autoUpdateCheckBox;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JToolBar genToolBar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JToolBar.Separator jSeparator2;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToggleButton landPotToggleButton;
    private javax.swing.JButton modelPopButton;
    private javax.swing.JButton nextStepButton;
    private javax.swing.JButton previousStepButton;
    private org.thema.common.swing.ProgressBarPanel progressBar;
    private javax.swing.JButton reinitButton;
    private javax.swing.JButton remButton;
    private javax.swing.JTextArea statTextArea;
    private javax.swing.JButton updateButton;
    private javax.swing.JComboBox viewComboBox;
    // End of variables declaration//GEN-END:variables
}
