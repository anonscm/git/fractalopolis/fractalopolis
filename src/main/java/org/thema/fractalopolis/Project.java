/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.locationtech.jts.geom.Envelope;
import java.awt.Color;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.coverage.grid.GridCoverageFactory;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.feature.SchemaException;
import org.geotools.gce.geotiff.GeoTiffWriter;
import org.geotools.geometry.Envelope2D;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.thema.common.Config;
import org.thema.common.ProgressBar;
import org.thema.common.Util;
import org.thema.common.swing.TaskMonitor;
import org.thema.data.IOFeature;
import org.thema.data.IOImage;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.DefaultFeatureCoverage;
import org.thema.data.feature.Feature;
import org.thema.data.feature.FeatureFilter;
import org.thema.data.feature.FeatureGetter;
import org.thema.drawshape.layer.ShapeFileLayer;
import org.thema.drawshape.style.FeatureStyle;
import org.thema.drawshape.style.LineStyle;
import org.thema.drawshape.style.PointStyle;
import org.thema.fractalopolis.access.Accessibilities;
import org.thema.fractalopolis.access.Facility;
import org.thema.fractalopolis.evaluator.BuildPopEvaluator;
import org.thema.fractalopolis.evaluator.Evaluator;
import org.thema.fractalopolis.evaluator.RestrictEvaluator;
import org.thema.fractalopolis.ifs.AffineTransformConverter;
import org.thema.fractalopolis.ifs.Fractal;
import org.thema.fractalopolis.ifs.Fractal.Scale;
import org.thema.fractalopolis.ifs.FractalElem;
import org.thema.fractalopolis.ifs.Ifs;
import org.thema.graph.SpatialGraph;
import org.thema.process.Rasterizer;


/**
 *
 * @author Gilles Vuidel
 */
public final class Project {
    
    public static Project project;
    
    public static final String HOUSING_ATTR = "housing";
    public static final String HEIGHT_ATTR = "height";
    
    private static final String BUILD_RASTER_FILE = "build.tif";
    
    public enum Layers {
        HOUSING, BUILD, ROAD, MICRO_BUILD, MICRO_ROAD, TRAIN, TRAIN_STATION, BUS, BUS_STATION, SHOP, GREEN, LEISURE, WORK, RESTRICT,
        RESTRICT_SLOPE, WATER, AGRICULTURE
    }
    
    public static final List<LayerDef> LAYERS = Arrays.asList(
            new LayerDef(Layers.MICRO_BUILD.toString(), "Micro buildings", new FeatureStyle(Color.black, Color.black), HEIGHT_ATTR, Number.class),
            new LayerDef(Layers.ROAD.toString(), "Road network", new LineStyle(new Color(203, 1, 0), 2)),
            new LayerDef(Layers.MICRO_ROAD.toString(), "Micro road network", new LineStyle(Color.black), "speed", Number.class),
            new LayerDef(Layers.BUS.toString(), "Bus network", new LineStyle(new Color(252, 203, 2), 2), "time", Number.class),
            new LayerDef(Layers.BUS_STATION.toString(), "Bus stations", new PointStyle(Color.black, Color.red)), 
            new LayerDef(Layers.TRAIN.toString(), "Train network", new LineStyle(new Color(76, 0, 155), 2), "time", Number.class),
            new LayerDef(Layers.TRAIN_STATION.toString(), "Train stations", new PointStyle(Color.black, Color.red)), 
            new LayerDef(Layers.SHOP.toString(), "Shops", new PointStyle(Color.yellow.darker()), Facility.FREQ_FIELD, Number.class, Facility.TYPE_FIELD, Object.class),
            new LayerDef(Layers.LEISURE.toString(), "Leisures", new PointStyle(Color.blue), Facility.FREQ_FIELD, Number.class, Facility.TYPE_FIELD, Object.class),
            new LayerDef(Layers.GREEN.toString(), "Green areas", new FeatureStyle(new Color(118, 179, 0), new Color(170, 203, 0))),
            new LayerDef(Layers.WORK.toString(), "Working areas", new FeatureStyle(Color.DARK_GRAY, Color.gray), "job", Number.class),
            new LayerDef(Layers.RESTRICT.toString(), "Restricted areas", new FeatureStyle(Color.orange, Color.gray)),
            new LayerDef(Layers.RESTRICT_SLOPE.toString(), "Restricted slope", new FeatureStyle(Color.gray, Color.gray)),
            new LayerDef(Layers.WATER.toString(), "Water", new FeatureStyle(new Color(1, 155, 255), new Color(1, 155, 255))),
            new LayerDef(Layers.AGRICULTURE.toString(), "Agriculture", new FeatureStyle(Color.YELLOW.darker(), Color.gray))
            );
    
    
    private Fractal largeFractal, fineFractal, currentFractal;
    private Accessibilities largeAccess, fineAccess;
    
    private String name;
    
    private List<ShapeFileLayer> infoLayers;
    
    
    private transient File projectFile;
    private transient boolean modify;

    private transient HashMap<String, DefaultFeatureCoverage<DefaultFeature>> coverages, fineScaleCoverages;
    private transient GridCoverage2D buildRaster;
    private transient SpatialGraph spatialGraph;

    /** Creates a new instance of Project */
    private Project(String name, File dir) throws IOException {
        this.name = name;
        this.projectFile = new File(dir, "project.xml");
        Ifs ifs = new Ifs();
        ifs.addTransform(new AffineTransform());
        modify = true;
        project = this;
        
        largeAccess = new Accessibilities(this, Scale.MACRO);
        fineAccess = new Accessibilities(this, Scale.MICRO);
        List<Evaluator> evaluators = new ArrayList<>();
        evaluators.add(new BuildPopEvaluator(this,Layers.BUILD.toString()));
        evaluators.add(new RestrictEvaluator(this));
        evaluators.add(largeAccess);
        
        ReferencedEnvelope env = new ShapefileDataStore(new File(dir, Layers.BUILD+".shp").toURL()).getFeatureSource().getBounds();
        double size = Math.min(env.getWidth(), env.getHeight());
        AffineTransform trans = new AffineTransform(size, 0, 0, size, env.getMinX()+(env.getWidth()-size)/2, env.getMinY()+(env.getHeight()-size)/2);
        coverages = new HashMap<>();
        fineScaleCoverages = new HashMap<>();
        
        largeFractal = new Fractal(ifs, trans, Scale.MACRO, "", evaluators);
        currentFractal = largeFractal;
    }
    
    public GridCoverage2D getBuildRaster() throws IOException {
        if(buildRaster == null) {
            buildRaster = IOImage.loadTiff(new File(getDirectory(), BUILD_RASTER_FILE));
        }
        
        return buildRaster;
    }

    public List<ShapeFileLayer> getInfoLayers() {
        if(infoLayers == null) {
            infoLayers = new ArrayList<>();
        }
        return infoLayers;
    }
    
    public void addInfoLayer(File f) throws IOException {
        getInfoLayers().add(new ShapeFileLayer(f));
    }
    
    public FeatureGetter<DefaultFeature> getLayerFeatures(final String name) {
        return new FeatureGetter<DefaultFeature>() {
            @Override
            public Collection<DefaultFeature> getFeatures() {
                    return getCoverage(name).getFeatures();
            }
        };
    }
    
    public DefaultFeatureCoverage<DefaultFeature> getCoverageLevel(String name, final int level) {
        String levelName = name + level;
        if(!coverages.containsKey(levelName)) {
            coverages.put(levelName, new DefaultFeatureCoverage<>(getCoverage(name).getFeatures(new FeatureFilter() {
                            @Override
                            public boolean accept(Feature f) {
                                return ((Number)f.getAttribute(Facility.FREQ_FIELD)).intValue() == level;
                            }
                        })));
        }
        
        return coverages.get(levelName);
    }
    
    public synchronized DefaultFeatureCoverage<DefaultFeature> getCoverage(String name) {
        if(!coverages.containsKey(name)) {
            try {
                coverages.put(name, new DefaultFeatureCoverage<>(IOFeature.loadFeatures(new File(getDirectory(), name+".shp"))));
            }catch (IOException ex) {
                Logger.getLogger(Project.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return coverages.get(name);
    }
    
    public synchronized DefaultFeatureCoverage<DefaultFeature> getFineScaleCoverage(String name) {
        if(!fineScaleCoverages.containsKey(name)) {
            DefaultFeatureCoverage<DefaultFeature> cov = getCoverage(name);
            Envelope env = new Envelope(fineFractal.getFractalModel().getRootElem().getGeometry().getEnvelopeInternal());
            env.expandBy(1000);
            fineScaleCoverages.put(name, new DefaultFeatureCoverage<>(cov.getFeatures(env)));
        }
        
        return fineScaleCoverages.get(name);
    }
    
    public boolean isLayerExist(String name) {
        return new File(getDirectory(), name+".shp").exists();
    }
    
    public synchronized SpatialGraph getSpatialGraph() {
        if(spatialGraph == null) {
            List<DefaultFeature> roads = getCoverage(Layers.MICRO_ROAD.toString()).getFeatures();
            spatialGraph = new SpatialGraph(roads);
        }
        
        return spatialGraph;
    }

    public File getProjectFile() {
        return projectFile;
    }
    
    public Fractal getLargeFractal() {
        return largeFractal;
    }

    public Fractal getFineFractal() {
        return fineFractal;
    }
    
    public Fractal getCurrentFractal() {
        return currentFractal;
    }

    public boolean isLargeCurrent() {
        return getCurrentFractal() == getLargeFractal();
    }
    
    public void setCurrentFractal(Fractal currentFractal) {
        this.currentFractal = currentFractal;
    }

    public Accessibilities getFineAccess() {
        return fineAccess;
    }

    public Accessibilities getLargeAccess() {
        return largeAccess;
    }

    public void save(String fileName) throws IOException {
        
        File file = fileName == null ? getProjectFile() : new File(getDirectory(), fileName);
        XStream xml = Util.getXStream();
        xml.registerConverter(new AffineTransformConverter());
        xml.alias("Project", Project.class);
        xml.alias("Transform", AffineTransform.class);

        try (FileWriter fw = new FileWriter(file)) {
            xml.toXML(this, fw);
        }
        projectFile = file;
        modify = false;
    }
    
    public boolean isModified() { 
        return modify; 
    }
    
    public File getDirectory() {
        return projectFile.getParentFile();
    }
    
    public static Project readProject(File projectFile) throws IOException {
        XStream xml = Util.getXStream();
        xml.registerConverter(new AffineTransformConverter());
        xml.alias("Project", Project.class);
        xml.alias("Transform", AffineTransform.class);

        Project prj;
        try (FileReader fr = new FileReader(projectFile)) {
            prj = (Project)xml.fromXML(fr);
        }
        
        return prj;
    }
    
    public static Project loadProject(File projectFile) throws IOException {
        Project prj = readProject(projectFile);
        
        prj.coverages = new HashMap<>();
        prj.fineScaleCoverages = new HashMap<>();
        prj.modify = false;
        prj.projectFile = projectFile;
        project = prj;
        return project;
    }
    
    public void importFractal(File projectFile) throws IOException {
        Project prj = readProject(projectFile);
        largeFractal = new Fractal(prj.getLargeFractal(), largeFractal.getEvaluators());
    }
    
    public static Project createProject(String name, File dir, File buildFile, String heightAttr, File popFile, String popAttr,
            TaskMonitor mon) throws IOException, SchemaException  {
        File directory = new File(dir, name);
        directory.mkdir();
        mon.setProgress(1);
        mon.setNote("Loading data...");
        List<DefaultFeature> features = IOFeature.loadFeatures(buildFile);
        List<DefaultFeature> buildFeatures = new ArrayList<>();
        List<String> attrNames = Arrays.asList(HOUSING_ATTR, HEIGHT_ATTR);
        for(Feature f : features) {
            buildFeatures.add(new DefaultFeature(f.getId(), f.getGeometry(), attrNames, 
                    Arrays.asList(0.0, heightAttr != null ? ((Number)f.getAttribute(heightAttr)).doubleValue() : 1.0)));
        }
        features = null;
        List<DefaultFeature> popFeatures = IOFeature.loadFeatures(popFile);
        if(!HOUSING_ATTR.equals(popAttr)) {
            DefaultFeature.addAttribute(HOUSING_ATTR, popFeatures, 0.0);
            for(DefaultFeature f : popFeatures) {
                f.setAttribute(HOUSING_ATTR, ((Number)f.getAttribute(popAttr)).doubleValue());
            }
        }
        mon.setNote("Spread housings...");
        DefaultFeatureCoverage<DefaultFeature> buildCov = new DefaultFeatureCoverage(buildFeatures);
        mon.setMaximum(popFeatures.size()+3);
        spreadHousing(popFeatures, buildCov, mon);
        
        CoordinateReferenceSystem crs = new ShapefileDataStore(buildFile.toURL()).getSchema().getCoordinateReferenceSystem();
        Rasterizer rasterizer = new Rasterizer(buildCov, 100);
        rasterizer.setPolygonalRasterization(Rasterizer.PolyRasterMode.INTERSECT);
        Raster buildRaster = rasterizer.rasterize(mon.getSubMonitor(0, 100, 1));
        Rectangle2D env = rasterizer.getEnvelope();
        GridCoverage2D gridCov = new GridCoverageFactory().create("build",
                (WritableRaster)buildRaster, new Envelope2D(crs, env.getMinX(), env.getMinY(), env.getWidth(), env.getHeight()));

        IOImage.saveTiffCoverage(new File(directory, BUILD_RASTER_FILE), gridCov);
        
        mon.setNote("Saving data...");
        
        IOFeature.saveFeatures(buildFeatures, new File(directory, Layers.BUILD+".shp"), crs);
        IOFeature.saveFeatures(popFeatures, new File(directory, Layers.HOUSING+".shp"), crs);
        Project prj =  new Project(name, directory);
        prj.save(null);
        return prj;
    }

    private static void spreadHousing(List<DefaultFeature> popFeatures, DefaultFeatureCoverage<DefaultFeature> buildCov, ProgressBar mon) {
        for(Feature zone : popFeatures) {
            List<DefaultFeature> builds = buildCov.getFeaturesIn(zone.getGeometry());
            double volume = 0;
            for(Feature build : builds) {
                volume += build.getGeometry().intersection(zone.getGeometry()).getArea() * ((Number)build.getAttribute(HEIGHT_ATTR)).doubleValue();
            }
            
            double density = ((Number)zone.getAttribute(HOUSING_ATTR)).doubleValue() / volume;
            for(DefaultFeature build : builds) {
                double pop = ((Number)build.getAttribute(HOUSING_ATTR)).doubleValue();
                build.setAttribute(HOUSING_ATTR, pop + density * build.getGeometry().intersection(zone.getGeometry()).getArea() * 
                            ((Number)build.getAttribute(HEIGHT_ATTR)).doubleValue());
            }
            mon.incProgress(1);
        }
    }
    
    public String getName() {
        return name;
    }

    public void createFineScale(FractalElem zone) {
        List<Evaluator> evaluators = new ArrayList<>();
        String buildLayer = isLayerExist(Layers.MICRO_BUILD.toString()) ? Layers.MICRO_BUILD.toString() : Layers.BUILD.toString();
        evaluators.add(new BuildPopEvaluator(this, buildLayer));
        evaluators.add(fineAccess);
        evaluators.addAll(fineAccess.getAccessibilities());
        fineScaleCoverages.clear();
        fineFractal = new Fractal(new Ifs(largeFractal.getIfs()), zone.getTransform(), Scale.MICRO, zone.getCode(), evaluators);
    }
    
    public void removeFineScale() {
        fineFractal = null;
        fineScaleCoverages.clear();
    }
    
    public void setLayer(LayerDef layer, File file, List<String> attrs) throws IOException  {
        ProgressBar mon = Config.getProgressBar("Create layer...", 2);
        mon.setNote("Loading...");
        List<DefaultFeature> features = IOFeature.loadFeatures(file);
        if(!layer.getAttrNames().isEmpty()) {
            for(String attr : layer.getAttrNames()) {
                if (!features.get(0).getAttributeNames().contains(attr)) {
                    DefaultFeature.addAttribute(attr, features, null);
                }
            }
            for(DefaultFeature f : features) {
                for (int i = 0; i < layer.getAttrNames().size(); i++) {
                    f.setAttribute(layer.getAttrNames().get(i), f.getAttribute(attrs.get(i)));
                }
            }
        }
        mon.incProgress(1);
        
        if(layer.getName().equals(Layers.MICRO_BUILD.toString())) {
            mon.setNote("Spread housing...");
            DefaultFeature.addAttribute(HOUSING_ATTR, features, 0);
            spreadHousing(getCoverage(Layers.HOUSING.toString()).getFeatures(), 
                    new DefaultFeatureCoverage<>(features), mon.getSubProgress(1));
        }
        
        mon.setNote("Saving...");
        IOFeature.saveFeatures(features, new File(getDirectory(), layer.getName() + ".shp"));
        mon.close();
        
        if(coverages.containsKey(layer.getName())) {
            coverages.remove(layer.getName());
        }
        if(fineScaleCoverages.containsKey(layer.getName())) {
            fineScaleCoverages.remove(layer.getName());
        }
    }

    
}
