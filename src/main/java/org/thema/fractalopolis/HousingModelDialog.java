/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.thema.common.collection.HashMap2D;
import org.thema.common.collection.TableModelHashMap2D;
import org.thema.fractalopolis.ifs.Fractal;
import org.thema.fractalopolis.ifs.FractalElem;

/**
 *
 * @author gvuidel
 */
public class HousingModelDialog extends javax.swing.JDialog {

    private Fractal fractal;
    private HashMap2D<String, Integer, Double> userNormCoefs;
    
    /**
     * Creates new form PopModelDialog
     */
    public HousingModelDialog(java.awt.Frame parent, Fractal fractal) {
        super(parent, false);
        initComponents();
        
        this.fractal = fractal;
        fillTable();
        updateStatTable();
        popSpinner.setValue((int)fractal.getUserPop());
        
        urbanTable.getModel().addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                if(e.getColumn() != 2) {
                    return;
                }
                double val = (Double)urbanTable.getValueAt(e.getFirstRow(), e.getColumn());
                if(val > 1) {
                    urbanTable.setValueAt(1.0, e.getFirstRow(), e.getColumn());
                }
                if(val < 0) {
                    urbanTable.setValueAt(0.0, e.getFirstRow(), e.getColumn());
                }
            }
        });
        modelTable.getModel().addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                // when a value changes, modify another value for normalization
                int row = e.getFirstRow();
                TableModel table = modelTable.getModel();
                int rank = Integer.parseInt(table.getColumnName(e.getColumn()));
                String step = table.getValueAt(row, 0).toString();
                double val = (Double)table.getValueAt(row, e.getColumn());
                if(table.getColumnCount() == 2 && val != 1) {
                    table.setValueAt(1.0, row, e.getColumn());
                    return;
                }

                if(val * HousingModelDialog.this.fractal.getUserModel().getNbRanks(rank) > 1) {
                    val = 1.0 / HousingModelDialog.this.fractal.getUserModel().getNbRanks(rank);
                    table.setValueAt(val, row, e.getColumn());
                    return;
                }
                
                double sum = 0;
                for(int r : userNormCoefs.getKeys2()) {
                    sum += userNormCoefs.getValue(step, r)
                            * HousingModelDialog.this.fractal.getUserModel().getNbRanks(r);
                }
                if(sum == 1) {
                    return;
                }
                // find another column to modify
                int col = e.getColumn() == 1 ? 2 : 1;
                rank = Integer.parseInt(table.getColumnName(col));
                val = (Double)table.getValueAt(row, col);
                userNormCoefs.setValue(step, rank, val + (1-sum)/HousingModelDialog.this.fractal.getUserModel().getNbRanks(rank));
                modelTable.clearSelection();
                //table.setValueAt(val + (1-sum)/HousingModelDialog.this.fractal.getUserModel().getNbRanks(rank), row, col);
            }
        });
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                doClose();
            }
        });
    }
    
    private void fillTable() {
        LinkedHashMap<String, Double> urbanEmpCoefs = fractal.getRegModel().getUrbanCoefs();
        LinkedHashMap<String, Double> urbanCoefs = fractal.getUserModel().getUrbanCoefs();
        for(String step : urbanCoefs.keySet()) {
            ((DefaultTableModel)urbanTable.getModel()).addRow(new Object[] {step, urbanEmpCoefs.get(step), urbanCoefs.get(step)});
        }
        userNormCoefs = fractal.getUserModel().getNormCoefs();
        TableModelHashMap2D model = new TableModelHashMap2D(userNormCoefs);
        model.setRowLabel("Step");
        modelTable.setModel(model);
        
        model = new TableModelHashMap2D(fractal.getRegModel().getNormCoefs()) {
            @Override
            public Class<?> getColumnClass(int columnIndex) {
                return String.class;
            }
            
        };
        model.setRowLabel("Step");
        empTable.setModel(model);
       
    }
    
    private void updateStatTable() {
        DefaultTableModel table = (DefaultTableModel) statTable.getModel();
        table.setRowCount(0);
        for(int step = 0; step <= fractal.getFractalModel().getNbIteration(); step++) {
            TreeMap<String, SummaryStatistics> statEmp = new TreeMap<>();
            TreeMap<String, SummaryStatistics> statModel = new TreeMap<>();
            for(FractalElem elem : fractal.getFractalModel().getIteration(step)) {
                String code = elem.getCode();
                if(!statEmp.containsKey(code)) {
                    statEmp.put(code, new SummaryStatistics());
                    statModel.put(code, new SummaryStatistics());
                }
                statEmp.get(code).addValue(elem.getHousing());
                statModel.get(code).addValue(elem.getUser());
            }
            for(String code : statEmp.descendingKeySet()) {
                SummaryStatistics emp = statEmp.get(code);
                SummaryStatistics model = statModel.get(code);
                table.addRow(new Object[] {step, code, FractalElem.getLevel(code), emp.getN(), emp.getSum(), emp.getMean(), emp.getMin(), emp.getMax(),
                    model.getSum(), model.getMean()});
            }
        }
       
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        closeButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        urbanTable = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        modelTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        popSpinner = new javax.swing.JSpinner();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        empTable = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        statTable = new javax.swing.JTable();
        updateButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Housing model");
        setAlwaysOnTop(true);
        setLocationByPlatform(true);

        closeButton.setText("Close");
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });

        urbanTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "", "Empirical", "Model"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(urbanTable);

        modelTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(modelTable);

        jLabel1.setText("Total housings");

        popSpinner.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));

        jLabel2.setText("Urban / Rural parameters");

        jLabel3.setText("Model parameters");

        empTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        empTable.setEnabled(false);
        jScrollPane3.setViewportView(empTable);

        jLabel4.setText("Empirical parameters");

        statTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Step", "Code", "Level", "Nb", "Total emp", "Avg emp", "Min emp", "Max emp", "Total model", "Avg model"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane4.setViewportView(statTable);

        updateButton.setText("Update table");
        updateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
                                .addGap(36, 36, 36))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
                                .addGap(17, 17, 17)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                            .addComponent(jLabel4))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(closeButton, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel3)
                                        .addGap(0, 83, Short.MAX_VALUE))
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))))
                    .addComponent(jScrollPane4)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(popSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(updateButton)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 132, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(popSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(updateButton))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(closeButton)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeButtonActionPerformed
        updateButtonActionPerformed(evt);
        doClose();
    }//GEN-LAST:event_closeButtonActionPerformed

    private void updateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateButtonActionPerformed
        Map<String, Double> urbanCoefs = new HashMap<>();
        TableModel table = urbanTable.getModel();
        for(int row = 0; row < table.getRowCount(); row++) {
            urbanCoefs.put((String)table.getValueAt(row, 0), (Double)table.getValueAt(row, 2));
        }
        fractal.getUserModel().setCoefs(urbanCoefs, userNormCoefs);
        fractal.setUserPop(((Number)popSpinner.getValue()).doubleValue());
        updateStatTable();
    }//GEN-LAST:event_updateButtonActionPerformed

   
    private void doClose() {
        setVisible(false);
        dispose();
    }

 
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton closeButton;
    private javax.swing.JTable empTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTable modelTable;
    private javax.swing.JSpinner popSpinner;
    private javax.swing.JTable statTable;
    private javax.swing.JButton updateButton;
    private javax.swing.JTable urbanTable;
    // End of variables declaration//GEN-END:variables
}
