/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.fractalopolis;


import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.geom.impl.CoordinateArraySequence;
import org.locationtech.jts.geom.util.AffineTransformation;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import org.geotools.geometry.jts.JTS;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.MultiGraph;
import org.graphstream.ui.geom.Point3;
import org.graphstream.ui.graphicGraph.GraphicGraph;
import org.graphstream.ui.graphicGraph.GraphicNode;
import org.graphstream.ui.swingViewer.DefaultView;
import org.graphstream.ui.swingViewer.LayerRenderer;
import org.graphstream.ui.swingViewer.View;
import org.graphstream.ui.swingViewer.Viewer;
import static org.graphstream.ui.swingViewer.Viewer.DEFAULT_VIEW_ID;
import static org.graphstream.ui.swingViewer.Viewer.newGraphRenderer;
import org.graphstream.ui.swingViewer.ViewerListener;
import org.graphstream.ui.swingViewer.ViewerPipe;
import org.thema.drawshape.PanelMap;
import org.thema.fractalopolis.ifs.FractalElemShape;

/**
 *
 * @author gvuidel
 */
public class EditShapeDialog extends javax.swing.JDialog implements MouseListener, ViewerListener {

    public boolean isOk = false;

    private final ArrayList<Point2D> points = new ArrayList();
    private Graph graph;
    private View view;
    private Viewer viewer;
    private ViewerPipe viewerPipe;
    private Node NodeSelected = null;
    private Polygon initPoly;
    private FractalElemShape fractalShape;
    
    /** Creates new form EditShapeDailog */
    public EditShapeDialog(java.awt.Frame parent, final FractalElemShape fractalShape, final PanelMap map) {
        super(parent, true);
        initComponents();
        setLocationRelativeTo(parent);
        getRootPane().setDefaultButton(okButton);
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                doClose();
            }
        });
        
        this.fractalShape = fractalShape;
        initPoly = new Polygon((LinearRing)JTS.toGeometry(fractalShape.getShape()), new LinearRing[0], new GeometryFactory());
        for(Coordinate c : initPoly.getCoordinates()){
            points.add(new Point2D.Double(c.x, c.y));
        }
        while(points.size() > 3 && points.get(0).equals(points.get(points.size()-1))) {
            points.remove(points.size()-1);
        }
        initGraph();
        
        view.setBackLayerRenderer(new LayerRenderer() {
            @Override
            public void render(Graphics2D graphics, GraphicGraph graph, double px2Gu, int widthPx, int heightPx, double minXGu, double minYGu, double maxXGu, double maxYGu) {
                graphics.setColor(Color.white);
                graphics.fillRect(0, 0, widthPx, heightPx);
                Rectangle2D.Double rectGu = new Rectangle2D.Double(minXGu, minYGu, maxXGu-minXGu, maxYGu-minYGu);
                if(rectGu.isEmpty()) {
                    return;
                }
                
                Point3 pmin = view.getCamera().transformPxToGu(0, heightPx);
                Point3 pmax = view.getCamera().transformPxToGu(widthPx, 0);
                rectGu = new Rectangle2D.Double(pmin.x, pmin.y, pmax.x-pmin.x, pmax.y-pmin.y);
                Rectangle2D clip = fractalShape.getTransform().createTransformedShape(rectGu).getBounds2D();
                RenderedImage backImg = map.getRenderedImage(clip, widthPx);
                graphics.drawRenderedImage(backImg, new AffineTransform());
            }
        });
        
        dessinPolygon();
        
        view.getCamera().setAutoFitView(false);
        view.getCamera().setViewPercent(1.2);
        view.getCamera().setViewCenter(0.5, 0.5, 0);
        
        panelDraw.setLayout(new BorderLayout());
        panelDraw.add(view,BorderLayout.CENTER);
    }

     /**
     * Dessin du polygon de base
     */
    private void initGraph(){
        //panelDraw.removeAll();
        graph = new MultiGraph("graphe" );
        viewer = new Viewer(graph, Viewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);
        viewer.disableAutoLayout();
        viewerPipe = viewer.newViewerPipe();
        viewerPipe.addViewerListener(this);
        view = new DefaultView(viewer, DEFAULT_VIEW_ID,	Viewer.newGraphRenderer()) {
            @Override
            public void render(Graphics2D g) {
                // workaround to have correct camera positionning for backrenderer
                BufferedImage img = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
                Graphics2D g2 = (Graphics2D)img.getGraphics();
                super.render(g2); 
                
                // real code
                super.render(g);
            }
                                            
                                        };
        viewer.addView(view);
        view.addMouseListener(this);
        
    }
    
    
    private void dessinPolygon(){
        graph.addAttribute("ui.antialias");
        graph.addAttribute("ui.quality");
        //points
        for(int i = 0 ; i < points.size() ; i++){
            Point2D p = points.get(i);
            Node n = graph.addNode(i+"");
            n.addAttribute("ui.style", 
                    "fill-color: #58a830; size: 12px;"); 
            n.addAttribute("xy",p.getX(), p.getY());
        }
        //aretes
        for(int i = 0 ; i < points.size() ; i++){
            Edge e = graph.addEdge(i+"/"+(i==points.size()-1 ? 0 : (i+1)),
                    i+"",
                    (i==points.size()-1 ? 0 : (i+1))+"",
                    false);
            e.addAttribute("ui.style", 
                    "fill-color: rgb(0,51,102); size: 3px; text-color: rgb(0,100,255);");
        }
    }

    public Polygon getPolygon() {
        GeometryFactory geomFactory = new GeometryFactory();
        CoordinateArraySequence coords = new CoordinateArraySequence(graph.getNodeCount()+1);
        int i = 0;
        for(GraphicNode gn : (Collection<GraphicNode>)(Collection)viewer.getGraphicGraph().getNodeSet()) {
            coords.getCoordinate(i++).setCoordinate(new Coordinate(gn.getX(), gn.getY()));
        }
        coords.getCoordinate(i).setCoordinate(coords.getCoordinate(0));
        return new Polygon(new LinearRing(coords, geomFactory), new LinearRing[0], geomFactory);
    }
    
    public Polygon getScaledPolygon() {
        Polygon poly = getPolygon();
        double scaling = Math.sqrt(initPoly.getArea() / poly.getArea());
        poly.apply(AffineTransformation.scaleInstance(scaling, scaling, 0.5, 0.5));
        
        return poly;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        okButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        panelDraw = new javax.swing.JPanel();
        jToolBar3 = new javax.swing.JToolBar();
        addBut2 = new javax.swing.JButton();
        removeBut = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        updateAreaButton = new javax.swing.JButton();
        resetShapeButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Edit shape");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        okButton.setText("OK");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        panelDraw.setPreferredSize(new java.awt.Dimension(400, 500));

        org.jdesktop.layout.GroupLayout panelDrawLayout = new org.jdesktop.layout.GroupLayout(panelDraw);
        panelDraw.setLayout(panelDrawLayout);
        panelDrawLayout.setHorizontalGroup(
            panelDrawLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 0, Short.MAX_VALUE)
        );
        panelDrawLayout.setVerticalGroup(
            panelDrawLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 242, Short.MAX_VALUE)
        );

        jToolBar3.setBackground(new java.awt.Color(230, 230, 230));
        jToolBar3.setRollover(true);

        addBut2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/add.png"))); // NOI18N
        addBut2.setToolTipText("Add vertex");
        addBut2.setFocusable(false);
        addBut2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        addBut2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        addBut2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addBut2ActionPerformed(evt);
            }
        });
        jToolBar3.add(addBut2);

        removeBut.setIcon(new javax.swing.ImageIcon(getClass().getResource("/remove.png"))); // NOI18N
        removeBut.setToolTipText("Remove selected vertex");
        removeBut.setFocusable(false);
        removeBut.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        removeBut.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        removeBut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeButActionPerformed(evt);
            }
        });
        jToolBar3.add(removeBut);
        jToolBar3.add(jSeparator1);
        jToolBar3.add(filler1);

        updateAreaButton.setText("Update area");
        updateAreaButton.setFocusable(false);
        updateAreaButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        updateAreaButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        updateAreaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateAreaButtonActionPerformed(evt);
            }
        });
        jToolBar3.add(updateAreaButton);

        resetShapeButton.setText("Reset shape");
        resetShapeButton.setFocusable(false);
        resetShapeButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        resetShapeButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        resetShapeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetShapeButtonActionPerformed(evt);
            }
        });
        jToolBar3.add(resetShapeButton);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(okButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 67, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cancelButton)
                .addContainerGap())
            .add(jToolBar3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(panelDraw, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 442, Short.MAX_VALUE)
        );

        layout.linkSize(new java.awt.Component[] {cancelButton, okButton}, org.jdesktop.layout.GroupLayout.HORIZONTAL);

        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(jToolBar3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panelDraw, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(cancelButton)
                    .add(okButton))
                .addContainerGap())
        );

        getRootPane().setDefaultButton(okButton);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        isOk = true;
        doClose();
    }//GEN-LAST:event_okButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        doClose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    /** Closes the dialog */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        doClose();
    }//GEN-LAST:event_closeDialog

    private void addBut2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addBut2ActionPerformed
        points.clear();
        for(Node n : graph){
            GraphicNode gn = viewer.getGraphicGraph().getNode(n.getId());
            Double xp= (double)(((int)(gn.getX()*100))/(double)100);
            Double yp= (double)(((int)(gn.getY()*100))/(double)100);
            points.add(new Point2D.Double(xp, yp));
        }
        int newNodeIndex = (NodeSelected == null? 1 : (NodeSelected.getIndex() == graph.getNodeCount()-1 ? 0 :  NodeSelected.getIndex()+1));
        int prec = (newNodeIndex == 0 ? graph.getNodeCount()-1 : newNodeIndex - 1 );
        int suiv = (prec == graph.getNodeCount()-1 ? 0 : prec+1);
        GraphicNode gnPrec = viewer.getGraphicGraph().getNode(graph.getNode(prec).getId());
        GraphicNode gnSuiv = viewer.getGraphicGraph().getNode(graph.getNode(suiv).getId());
        double x = (double)(gnPrec.getX()+gnSuiv.getX())/(double)2;
        double y = (double)(gnPrec.getY()+gnSuiv.getY())/(double)2;
        points.add(newNodeIndex, new Point2D.Double(x, y));
        graph.clear();
        dessinPolygon();
        NodeSelected = graph.getNode(newNodeIndex);
    }//GEN-LAST:event_addBut2ActionPerformed

    private void removeButActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeButActionPerformed
        if(NodeSelected != null && graph.getNodeCount() > 2){
            points.clear();
            graph.removeNode(NodeSelected);
            ArrayList<Integer> l = new ArrayList();
            for(Node n : graph) {
                l.add(Integer.parseInt(n.getId()));
            }
            Collections.sort(l);
            for(Integer i : l){
                Node n = graph.getNode(i+"");
                GraphicNode gn = viewer.getGraphicGraph().getNode(n.getId());
                Double x= (double)(((int)(gn.getX()*100))/(double)100);
                Double y= (double)(((int)(gn.getY()*100))/(double)100);
                points.add(new Point2D.Double(x, y));
            }
            NodeSelected = null;
            graph.clear();
            dessinPolygon();
        }
    }//GEN-LAST:event_removeButActionPerformed

    private void updateAreaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateAreaButtonActionPerformed
        Polygon poly = getScaledPolygon();
        graph.clear();
        points.clear();
        for(Coordinate c : poly.getCoordinates()){
            points.add(new Point2D.Double(c.x, c.y));
        }
        while(points.size() > 3 && points.get(0).equals(points.get(points.size()-1))) {
            points.remove(points.size()-1);
        }

        dessinPolygon();
        view.repaint();
    }//GEN-LAST:event_updateAreaButtonActionPerformed

    private void resetShapeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetShapeButtonActionPerformed
        graph.clear();
        points.clear();

        Geometry geom = JTS.toGeometry(fractalShape.getFeature().getInitShape());
        for(Coordinate c : geom.getCoordinates()){
            points.add(new Point2D.Double(c.x, c.y));
        }
        while(points.size() > 3 && points.get(0).equals(points.get(points.size()-1))) {
            points.remove(points.size()-1);
        }
        dessinPolygon();
        view.repaint();
    }//GEN-LAST:event_resetShapeButtonActionPerformed

    private void doClose() {
        setVisible(false);
        dispose();
    }

    @Override
    public void mouseClicked(MouseEvent e) { 
        viewerPipe.pump();
    }
    @Override
    public void mousePressed(MouseEvent e) { 
        if(NodeSelected != null) {
            NodeSelected.addAttribute("ui.style","fill-color: #58a830;");
            NodeSelected = null;
        }
        viewerPipe.pump();
    }
    @Override
    public void mouseReleased(MouseEvent e){ 
        viewerPipe.pump(); 
    }
    @Override
    public void mouseEntered(MouseEvent e) {
    }
    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void viewClosed(String viewName) {
    }

    @Override
    public void buttonPushed(final String id) {
        NodeSelected = graph.getNode(id);
        NodeSelected.addAttribute("ui.style","fill-color: #a01010;");
    }
        
    @Override
    public void buttonReleased(String id) {
    }
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addBut;
    private javax.swing.JButton addBut1;
    private javax.swing.JButton addBut2;
    private javax.swing.JButton cancelButton;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JToolBar jToolBar3;
    private javax.swing.JButton okButton;
    private javax.swing.JPanel panelDraw;
    private javax.swing.JButton removeBut;
    private javax.swing.JButton resetShapeButton;
    private javax.swing.JButton updateAreaButton;
    // End of variables declaration//GEN-END:variables

}
