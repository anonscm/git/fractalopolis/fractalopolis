/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.thema.common.collection.HashMap2D;
import org.thema.fractalopolis.ifs.Fractal;
import org.thema.fractalopolis.ifs.FractalElem;
import org.thema.fractalopolis.ifs.FractalModel;
import org.thema.fractalopolis.ifs.Ifs;

/**
 *
 * @author gvuidel
 */
public class HousingModel {
    public static final String [] URBAN_NAME = new String [] {"α", "β", "γ", "δ" };
    public static final String [] STEP_NAME = new String [] {"a", "b", "c", "d" };
    
    private HashMap2D<String, Integer, Double> coefs;
    private HashMap<Integer, Integer> ranks;

    public HousingModel(Ifs ifs) {
        this.coefs = new HashMap2D<>(Arrays.asList(STEP_NAME[0]), ifs.getRanks(), 1.0);
        ranks = new HashMap<>();
        for(int i = 0; i < ifs.getNbTransform(); i++) {
            if (!ranks.containsKey(ifs.getIndCoef(i))) {
                ranks.put(ifs.getIndCoef(i), 1);
            } else {
                ranks.put(ifs.getIndCoef(i), 1 + ranks.get(ifs.getIndCoef(i)));
            }
        }
        
        for(int rank : coefs.getKeys2()) {
            coefs.setValue(STEP_NAME[0], rank, 1.0 / (coefs.getKeys2().size() * ranks.get(rank)));
        }
    }
    
    public HousingModel(Fractal fractal) {
        
        if(fractal.getFractalModel().getNbIteration() == 0) {
            throw new IllegalStateException("Fractal must have one step at least.");
        }
        
        FractalModel fractalModel = fractal.getFractalModel();
        Ifs ifs = fractal.getIfs();
        
        ranks = new HashMap<>();
        HashMap<Integer, List<double[]>> matrix = new HashMap<>();
        for(int i = 0; i < ifs.getNbTransform(); i++) {
            if (!matrix.containsKey(ifs.getIndCoef(i))) {
                matrix.put(ifs.getIndCoef(i), new ArrayList<double[]>());
                ranks.put(ifs.getIndCoef(i), 1);
            } else {
                ranks.put(ifs.getIndCoef(i), 1 + ranks.get(ifs.getIndCoef(i)));
            }
        }
        
        coefs = new HashMap2D<>(Arrays.asList(STEP_NAME[0]), matrix.keySet());
        
        for(int level = 1; level <= fractalModel.getNbIteration(); level++) {
            List<FractalElem> elems = fractalModel.getIteration(level);
            for(FractalElem elem : elems) {
                matrix.get(ifs.getIndCoef(elem.getIndIfs())).add(new double[] {elem.getParent().getHousing(), elem.getHousing()});
            }
            for(Integer ind : matrix.keySet()) {
                List<double[]> values = matrix.get(ind);
                double sumPopParent = 0;
                double sumPopElem = 0;
                for(double [] pop : values) {
                    sumPopParent += pop[0];
                    sumPopElem += pop[1];
                }
                double coef = sumPopElem / sumPopParent;
                coefs.setValue(STEP_NAME[level-1], ind, coef);
                
                values.clear();
            }
        }
        
    }

//    public void updateRanks(Ifs ifs) {
//        ranks = new HashMap<>();
//        for(int i = 0; i < ifs.getNbTransform(); i++) {
//            if (!ranks.containsKey(ifs.getIndCoef(i))) {
//                ranks.put(ifs.getIndCoef(i), 1);
//            } else {
//                ranks.put(ifs.getIndCoef(i), 1 + ranks.get(ifs.getIndCoef(i)));
//            }
//            coefs.addKey2(ifs.getIndCoef(i));
//        }
//    }
    
    public void addStep(int step) {
        if(coefs.getKeys1().contains(STEP_NAME[step-1])) {
            return;
        }
        coefs.addKey1(STEP_NAME[step-1]);
        if(step > 1) {
            for(int rank : coefs.getKeys2()) {
                coefs.setValue(STEP_NAME[step-1], rank, coefs.getValue(STEP_NAME[step-2], rank));
            }
        }
    }

    public double getCoef(int step, int rank) {
        return coefs.getValue(STEP_NAME[step-1], rank);
    }
    
    public LinkedHashMap<String, Double> getUrbanCoefs() {
        LinkedHashMap<String, Double> urbanCoef = new LinkedHashMap<>();
        
        for(String step : coefs.getKeys1()) {
            double coef = 0;
            for(Integer rank : coefs.getKeys2()) {
                coef += coefs.getValue(step, rank) * getNbRanks(rank);
            }
            urbanCoef.put(getUrbanName(step), coef);
        }
        
        return urbanCoef;
    }
    
    public HashMap2D<String, Integer, Double> getNormCoefs() {
        LinkedHashMap<String, Double> urbanCoefs = getUrbanCoefs();
        HashMap2D<String, Integer, Double> normCoef = new HashMap2D<>(coefs.getKeys1(), coefs.getKeys2(), 0.0);
        for(String step : coefs.getKeys1()) {
            for(Integer rank : coefs.getKeys2()) {
                normCoef.setValue(step, rank, coefs.getValue(step, rank) / urbanCoefs.get(getUrbanName(step)));
            }
        }
        
        return normCoef;
    }

    public void setCoefs(Map<String, Double> urbanCoefs, HashMap2D<String, Integer, Double> normCoefs) {
        for(String step : coefs.getKeys1()) {
            for(Integer rank : coefs.getKeys2()) {
                coefs.setValue(step, rank, normCoefs.getValue(step, rank) * urbanCoefs.get(getUrbanName(step)));
            }
        }
    }

    public int getNbRanks(int rank) {
        return ranks.get(rank);
    }

    private String getUrbanName(String stepName) {
        return URBAN_NAME[Arrays.asList(STEP_NAME).indexOf(stepName)];
    }
    
}
