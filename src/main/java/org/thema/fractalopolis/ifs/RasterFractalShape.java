/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis.ifs;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import org.thema.drawshape.AbstractDrawableShape;

/**
 *
 * @author gvuidel
 */
public class RasterFractalShape extends AbstractDrawableShape implements Runnable, TableModelListener {
    
    protected Ifs ifs;
    protected BufferedImage image;
    protected Dimension size;
    private Point2D.Double p;
    private Thread thread;
    private boolean stop = false;
    private boolean ifsChanged = false;
    
    /** Creates a new instance of RasterFractalShape */
    public RasterFractalShape(Ifs i) {
        ifs  = i;
        size = new Dimension(800, 800);
        image = new BufferedImage((int)size.getWidth(), (int)size.getHeight(), BufferedImage.TYPE_INT_RGB);
        ifs.getTableModel().addTableModelListener(this);
    }

    @Override
    public Rectangle2D getBounds() {
        return new Rectangle2D.Double(0, 0, size.getWidth(), size.getHeight());
    }
    
    @Override
    public void draw(Graphics2D g, AffineTransform t) {
        g.drawImage(image, t, null);
    }
    
    public void generate() {
        if(thread != null) {
            stop = true;
        }
        while(stop) ;
        thread = new Thread(this);
        thread.setPriority(Thread.MIN_PRIORITY);
        thread.start();
    }
    
    private double[] calcProba() {
        double s = 0;
        double [] proba = new double[ifs.getNbTransform()];
        
        for(int i = 0; i < proba.length; i++) {
            proba[i] = Math.abs(ifs.getTransform(i).getDeterminant());
            s += proba[i];
        }
        
        for(int i = 0; i < proba.length; i++) {
            proba[i] /= s;
        }
        
        return proba;
    }

    @Override
    public void run() {
        double [] proba = calcProba();
        AffineTransform t = AffineTransform.getScaleInstance(size.getWidth(), size.getHeight());
        Point2D.Double pt = new Point2D.Double();
        boolean max = false;
        
        p = new Point2D.Double(0, 0);
        while(!stop && listenerList.getListenerCount() > 0) {
            for(int i = 0; i < size.getWidth()*size.getHeight()*0.2; i++) {
                double pr = Math.random();
                for(int k = 0; k < proba.length && pr != -1; k++) {
                    if (pr < proba[k]) {
                        AffineTransform ifst = ifs.getTransform(k);
                        if (ifst != null) {
                            ifst.transform(p, p);
                        }
                        t.transform(p, pt);
                        if (pt.x >= 0 && pt.x < size.width && pt.y >= 0 && pt.y < size.width) {
                            if ((image.getRGB((int)pt.x, (int)pt.y) & 0xff) == 0xf0) {
                                max = true;
                            } else if ((image.getRGB((int)pt.x, (int)pt.y) & 0xff00) == 0xf000) {
                                image.setRGB((int)pt.x, (int)pt.y, image.getRGB((int)pt.x, (int)pt.y)+0x10);
                            } else if ((image.getRGB((int)pt.x, (int)pt.y) & 0xff0000) == 0xf00000) {
                                image.setRGB((int)pt.x, (int)pt.y, image.getRGB((int)pt.x, (int)pt.y)+0x1000);
                            } else {
                                image.setRGB((int)pt.x, (int)pt.y, image.getRGB((int)pt.x, (int)pt.y)+0x100000);
                            }
                        }
                        pr = -1;
                    } else {
                        pr -= proba[k];
                    }
                }
                Thread.yield();
            }
            
            if(ifsChanged) {
                for(int y = 0; y < size.getHeight(); y++) {
                    for (int x = 0; x < size.getWidth(); x++) {
                        image.setRGB(x, y, 0);
                    }
                }
                proba = calcProba();
                p = new Point2D.Double(0, 0);
                ifsChanged = false;
                max = false;
            }
            fireShapeChanged();
            if(!stop && max && !ifsChanged) {
                try {
                    thread.join();
                } catch(InterruptedException ex) {
                    
                }
            }
            
        }
        stop = false;
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        ifsChanged = true;
        if(thread != null) {
            thread.interrupt();
        }
    }
}
