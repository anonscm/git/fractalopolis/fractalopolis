/*
 * Copyright (C) 2021 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.fractalopolis.ifs;

import org.locationtech.jts.geom.Geometry;
import org.thema.data.feature.Feature;
import org.thema.drawshape.feature.FeatureShape;

/**
 *
 * @author gvuidel
 */
public class RuralElemShape extends FeatureShape {
    
    public RuralElemShape(Feature f) {
        super(f);
    }

    @Override
    public Geometry getGeometry() {
        return getFeature().getGeometry(); 
    }
    
    
    
}
