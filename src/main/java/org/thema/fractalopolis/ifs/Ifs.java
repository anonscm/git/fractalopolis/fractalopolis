/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 
package org.thema.fractalopolis.ifs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.table.AbstractTableModel;
import org.thema.drawshape.style.SimpleStyle;
import org.thema.drawshape.style.Style;

/**
 *
 * @author Gilles Vuidel
 */
public class Ifs {

    public static class IfsElem {
        AffineTransform transform;
        Integer indCoef;

        public IfsElem(AffineTransform transform, Integer indCoef) {
            this.transform = transform;
            this.indCoef = indCoef;
        }

    }
    
    public static final Shape SQUARE = new Rectangle2D.Double(0, 0, 1, 1);
    public static final Shape CIRCLE = new Ellipse2D.Double(0, 0, 1, 1);
    
    public static List<Shape> shapes;
    static {
        shapes = new ArrayList<>();
        shapes.add(SQUARE);
        shapes.add(CIRCLE);
        
        final double r = 0.5;
        // Hexagon
        GeneralPath g = new GeneralPath();
        double a = Math.PI/2;
        g.moveTo(0.5+r*Math.cos(a), 0.5+r*Math.sin(a));
        for(int i = 0; i < 6; i++) {
            a += 2*Math.PI/6;
            g.lineTo(0.5+r*Math.cos(a), 0.5+r*Math.sin(a));
        }
        shapes.add(g);
        // Pentagon
        g = new GeneralPath();
        a = Math.PI/2;
        g.moveTo(0.5+r*Math.cos(a), 0.5+r*Math.sin(a));
        for(int i = 0; i < 5; i++) {
            a += 2*Math.PI/5;
            g.lineTo(0.5+r*Math.cos(a), 0.5+r*Math.sin(a));
        }
        shapes.add(g);
        // triangle
        g = new GeneralPath();
        g.moveTo(0, 0.0); g.lineTo(1, 0.0); g.lineTo(0.5, 0.866025404); g.lineTo(0, 0.0);
        shapes.add(g);
        
    }
    
    public static final Style style = new SimpleStyle(Color.BLACK, new Color(0, 0, 255, 127));
    
    private int indShape;
    private List<IfsElem> elems;
    
    private transient IfsLayer layer;
    private transient IfsTableModel table;

    /** Creates a new instance of Ifs */
    public Ifs() {
        elems = new ArrayList<>();
        indShape = 0;
    }
    
    /** Creates a new instance of Ifs */
    public Ifs(Ifs i) {
        indShape = i.indShape;
        elems = new ArrayList<>();
        for(IfsElem elem : i.elems) {
            elems.add(new IfsElem(new AffineTransform(elem.transform), elem.indCoef));
        }
    }
    
    public Dimension getOptimalImageSize(int nIter) {
        double minx = Double.POSITIVE_INFINITY;
        double miny = Double.POSITIVE_INFINITY;
        
        for(IfsElem elem : elems) {
            if(elem.transform.getScaleX() < minx) {
                minx = elem.transform.getScaleX();
            }
            if(elem.transform.getScaleY() < miny) {
                miny = elem.transform.getScaleY();
            }
        }
        
        return new Dimension((int)Math.pow(1/minx, nIter), (int)Math.pow(1/miny, nIter));
    }
    
    public int getNbTransform() {
        return elems.size();
    }
    
    public AffineTransform getTransform(int ind) {
        if(ind >= elems.size()) {
            return null;
        }
        return elems.get(ind).transform;
    }
    
    public void setTransform(AffineTransform t, int ind) {
        getLayer().setTransform(t, ind);
        elems.get(ind).transform = t;
        getTableModel().fireTableRowsUpdated(ind, ind);
    }
    
    public void addTransform(AffineTransform t) {
        getLayer().addShape(t);
        elems.add(new IfsElem(t, 0));
        getTableModel().fireTableRowsInserted(elems.size()-1, elems.size()-1);
    }

    public void removeTransform(int ind) {
        getLayer().removeShape(ind);
        elems.remove(ind);
        getTableModel().fireTableRowsDeleted(ind, ind);
    }
    
    public int getIndCoef(int ind) {
        return elems.get(ind).indCoef;
    }
    
    public void setIndCoef(int ind, int coef) {
        elems.get(ind).indCoef = coef;
    }
    
    public Set<Integer> getRanks() {
        HashSet<Integer> set = new HashSet<>();
        for(IfsElem elem : elems) {
            set.add(elem.indCoef);
        }
        return set;
    }
    
    public Shape getInitShape() {
        return shapes.get(indShape);
    }
    
    public void setInitShape(int indShape) {
        this.indShape = indShape;
        getLayer().updateShapes();
    }
    
    public synchronized IfsLayer getLayer() {
        if(layer == null) {
            layer = new IfsLayer(this, style);
        }
        return layer;
    }
    
    public synchronized IfsTableModel getTableModel() {
        if(table == null) {
            table = new IfsTableModel();
        }
        return table;
    }
    
    public void exportFile(String fileName) throws IOException {
        try (BufferedWriter f = new BufferedWriter(new FileWriter(fileName))) {
            for (int i = 0; i < getNbTransform(); i++) {
                for (int j = 0; j < 7; j++) {
                    f.write(String.valueOf(getTableModel().getValueAt(i, j)) + "\t");
                }
                f.newLine();
            }
        }
    }

    public void importFile(String fileName) throws Exception {
        while(!elems.isEmpty()) {
            removeTransform(0);
        }
        try (BufferedReader f = new BufferedReader(new FileReader(fileName))) {
           String line;
           int l = 0;
           while((line = f.readLine()) != null) {
               addTransform(new AffineTransform());
               String[] tokens = line.trim().split("\\s+");
               for(int i = 0; i < 6; i++) {
                   getTableModel().setValueAt(Double.parseDouble(tokens[i]), l, i);
               }
               l++;
           }
       }
           
       getTableModel().fireTableDataChanged();
    }
    
    @Override
    public boolean equals(Object o) {
        if(o == null || !(o instanceof Ifs)) {
            return false;
        }
        Ifs ifs = (Ifs)o;
        return ifs.elems.equals(elems);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.elems != null ? this.elems.hashCode() : 0);
        hash = 97 * hash + this.indShape ;
        return hash;
    }
    
    
    public class IfsTableModel extends AbstractTableModel {
        
        @Override
        public int getRowCount() {
            return elems.size();
        }

        @Override
        public int getColumnCount() {
            return 7;
        }

        @Override
        public void setValueAt(Object value, int row, int col) {
            if(col == 6) {
                setIndCoef(row, ((Number)value).intValue());
            } else {
                double [] m = new double[6];
                getTransform(row).getMatrix(m);

                m[col] = ((Double)value);
                setTransform(new AffineTransform(m), row);
            }
            fireTableCellUpdated(row, col);
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            if(columnIndex == 6) {
                return getIndCoef(rowIndex);
            } else {
                double [] m = new double[6];
                getTransform(rowIndex).getMatrix(m);
                return m[columnIndex];
            }
        }

        @Override
        public String getColumnName(int col) {
            switch(col) {
                case 0:
                    return "Sx";
                case 1:
                    return "Shx";
                case 2:
                    return "Shy";
                case 3:
                    return "Sy";
                case 4:
                    return "Tx";
                case 5:
                    return "Ty";
                case 6:
                    return "Rank";

            }

            return null;
        }

        @Override
        public Class getColumnClass(int c) {
            if(c == 6) {
                return Integer.class;
            } else {
                return Double.class;
            }
        }

        @Override
        public boolean isCellEditable(int row, int col) {
            return true;
        }
    }
}

