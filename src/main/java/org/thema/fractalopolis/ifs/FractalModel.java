/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis.ifs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 *
 * @author Gilles Vuidel
 */
public final class FractalModel {

    private List<List<FractalElem>> lstTrans;
    
    /** Creates a new instance of FractalVector */
    public FractalModel(FractalElem rootElem) {
        lstTrans = new ArrayList<>();
        init(rootElem);
    }
    
    public void init(FractalElem rootElem) {
        lstTrans.clear();
        List<FractalElem> init = new ArrayList<>();
        init.add(rootElem);
        lstTrans.add(init);
    }
    
    public void addIteration(List<FractalElem> iter) {
        lstTrans.add(iter);
    }
    
    public void removeIteration(int step) {
        if(step < 1) {
            return;
        }
        for(FractalElem elem : getIteration(step)) {
            elem.getParent().setChildren(Collections.EMPTY_LIST);
        }
        lstTrans = new ArrayList<>(lstTrans.subList(0, step));
    }
    
    public List<FractalElem> getLastIteration() {
        return lstTrans.get(lstTrans.size()-1);
    }
    
    public List<FractalElem> getIteration(int step) {
        return lstTrans.get(step);
    }
    
    public int getNbIteration() {
        return lstTrans.size()-1;
    }
    
    public FractalElem getRootElem() {
        return lstTrans.get(0).get(0);
    }

}
