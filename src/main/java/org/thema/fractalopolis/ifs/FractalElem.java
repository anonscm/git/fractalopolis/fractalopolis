/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis.ifs;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LinearRing;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.geotools.geometry.jts.JTS;
import org.thema.data.feature.AbstractFeature;
import org.thema.data.feature.AbstractSimpleFeature;

/**
 *
 * @author gvuidel
 */
public class FractalElem extends AbstractFeature {

    public static final List<String> ATTRNAMES = Arrays.asList(
            "code", "level", "size", "built_area", "building_land_potential", 
            "housing_empirical", "housing_density_empirical", "housing_avg_empirical", 
            "housing_model", "housing_density_model", "diff_model-empirical", "diff_avg_emp-empirical", "rate_model-empirical");
    
    public static final List<String> RURAL_ATTRNAMES = Arrays.asList(
            "code", "level", "area", "housing_empirical", "housing_density_empirical", 
            "housing_model", "housing_density_model", "diff_model-empirical", "rate_model-empirical");
    
    private List<String> attrNames, ruralAttrNames;
    private List<Double> attrValues, ruralAttrValues;
    
    private AffineTransform transform;
    private double housing, buildArea, user, restrictArea = 0;
    private FractalElem parent;
    private ArrayList<FractalElem> children;
    private Ifs ifs;
    private int indIfs;

    private Shape shape;
    
    private transient String code;
    private transient Geometry geom;
    
    public FractalElem(Ifs ifs, int indIfs, AffineTransform transform) {
        this.ifs = ifs;
        this.indIfs = indIfs;
        this.transform = transform;
        this.parent = null;
        children = new ArrayList<>(0);
        attrNames = new ArrayList<>();
        attrValues = new ArrayList<>();
        ruralAttrNames = new ArrayList<>();
        ruralAttrValues = new ArrayList<>();
    }
    
    public FractalElem(Ifs ifs, int indIfs, AffineTransform transform, String startCode) {
        this(ifs, indIfs, transform);
        code = startCode;
    }

    private void setParent(FractalElem parent) {
        this.parent = parent;
        code = null;
    }
    
    public void addChild(FractalElem elem) {
        children.add(elem);
        elem.setParent(this);
    }

    public void setChildren(List<FractalElem> children) {
        this.children.clear();
        for(FractalElem child : children) {
            addChild(child);
        }
        this.children.trimToSize();
    }

    public List<FractalElem> getChildren() {
        return Collections.unmodifiableList(children);
    }    

    public FractalElem getParent() {
        return parent;
    }
    
    public List<FractalElem> getBrothers() {
        if(getParent() == null) {
            return Collections.EMPTY_LIST;
        }
        List<FractalElem> brothers = new ArrayList<>(getParent().getChildren());
        brothers.remove(this);
        return brothers;
    }
    
    public List<FractalElem> getAllChildren() {
        List<FractalElem> elems = new ArrayList<>();
        elems.add(this);
        for(FractalElem e : children) {
            elems.addAll(e.getAllChildren());
        }
        return elems;
    }
    
    public List<FractalElem> getElemWithCode(String code) {
        if(getParent() == null) {
            List<FractalElem> elems = new ArrayList<>();
            for(FractalElem e : getAllChildren()) {
                if(e.getCode().equals(code)) {
                    elems.add(e);
                }
            }
            return elems;
        } else {
            return getParent().getElemWithCode(code);
        }
        
    }
    
    public int getStep() {
        if(parent == null) {
            return 0;
        }
        return parent.getStep()+1;
    }
    
    public String getRuralCode() {
        if(parent == null) {
            return "R";
        }
        return parent.getRuralCode() + getRank();
    }

    public String getCode() {
        if(code == null) {
            if(parent == null) {
                return "";
            }
            return parent.getCode() + getRank();
        } else {
            return code;
        }
    }
    
    public int getLevel() {
        return getLevel(getCode());
    }
    
    public double getBuildArea() {
        return buildArea;
    }

    public void setBuildArea(double buildArea) {
        this.buildArea = buildArea;
    }

    public void setRestrictArea(double restrictArea) {
        this.restrictArea = restrictArea;
    }

    public double getUrbanHousing() {
        if(getChildren().isEmpty()) {
            return getHousing();
        }
        
        double popChildren = 0;
        for(FractalElem elem : getChildren()) {
            popChildren += elem.getHousing();
        }
        return popChildren;
    }

    public double getRuralHousing() {
        return getHousing() - getUrbanHousing();
    }
    
    public double getRuralHousingUser() {
        if(getChildren().isEmpty()) {
            return 0;
        }
        double popChildren = 0;
        for(FractalElem elem : getChildren()) {
            popChildren += elem.getUser();
        }
        return getUser() - popChildren;
    }

    public double getAvgHousing() {
        return getElemWithCode(getCode()).stream().mapToDouble(e -> e.getHousing()).average().getAsDouble();
    }
    
    public double getHousing() {
        return housing;
    }

    public void setHousing(double housing) {
        this.housing = housing;
    }

    public double getUser() {
        return user;
    }

    public void setUser(double user) {
        this.user = user;
    }
    
    public Geometry getRuralGeometry() {
        if(children.isEmpty()) {
            return new GeometryFactory().buildGeometry(Collections.EMPTY_LIST);
        }
        Geometry g = getGeometry();
        for(FractalElem child : children) {
            g = g.difference(child.getGeometry());
        }
        return g;
    }
    
    public double getRuralRealArea() {
        if(children.isEmpty()) {
            return 0;
        }
        double area = getRealArea();
        for(FractalElem child : children) {
            area -= child.getRealArea();
        }
        return area;
    }
    
    public RuralFeature getRuralFeature() {
        if(children.isEmpty()) {
            return null;
        }
        return new RuralFeature();
    }

    public AffineTransform getTransform() {
        return transform;
    }

    public synchronized void setTransform(AffineTransform transform) {
        AffineTransform oldInv;
        try {
             oldInv = this.transform.createInverse();
        } catch (NoninvertibleTransformException ex) {
            throw new RuntimeException(ex);
        }
        this.transform.setTransform(transform);
        
        // si il y a des enfants les modifier en conséquence
   
        for(FractalElem elem : children) {
            AffineTransform trans = new AffineTransform(elem.getTransform());
            trans.preConcatenate(oldInv); trans.preConcatenate(transform);
            elem.setTransform(trans);
        }
        
        geom = null;
    }
    
    public Shape getInitShape() {
        return ifs.getInitShape();
    }

    public Shape getShape() {
        if(shape == null) {
            return ifs.getInitShape();
        } else {
            return shape;
        }
    }

    public void setShape(Shape shape) {
        this.shape = shape;
        this.geom = null;
    }

    public int getIndIfs() {
        return indIfs;
    }
    
    public int getRank() {
        return ifs.getIndCoef(indIfs);
    }
    
    public double getSize() {
        return Math.sqrt(getGeometry().getArea());
    }    
    
    public double getRealArea() {
        return getGeometry().getArea()-restrictArea;
    }
    
    public synchronized void setAttribute(String name, double val) {
        int ind = attrNames.indexOf(name);
        if(ind < 0) {
            attrNames.add(name);
            attrValues.add(val);
        } else {
            attrValues.set(ind, val);
        }
    }
    
    public synchronized void setRuralAttribute(String name, double val) {
        if(ruralAttrNames == null) {
            ruralAttrNames = new ArrayList<>();
            ruralAttrValues = new ArrayList<>();
        }
        int ind = ruralAttrNames.indexOf(name);
        if(ind < 0) {
            ruralAttrNames.add(name);
            ruralAttrValues.add(val);
        } else {
            ruralAttrValues.set(ind, val);
        }
    }

    @Override
    public Object getAttribute(int ind) {
        switch(ind) {
            case 0:
                return getCode();
            case 1:
                return getLevel();
            case 2:
                return getSize();
            case 3:
                return buildArea;
            case 4:
                double pot = 1 - 2 * restrictArea / getGeometry().getArea();
                if(pot < 0) {
                    return 0.0;
                }
                return pot;    
            case 5:
                return housing;
            case 6:
                return housing / (getRealArea() / 10000);
            case 7:
                return getAvgHousing();
            case 8:
                return user;
            case 9:
                return user / (getRealArea() / 10000);
            case 10:
                return user-getHousing();
            case 11:
                return getAvgHousing()-getHousing();
            case 12:
                return 100 * (user-housing) / (housing+1);
            default:
                return attrValues.get(ind-ATTRNAMES.size());
        }

    }

    @Override
    public Object getAttribute(String name) {
        int ind = ATTRNAMES.indexOf(name);
        if(ind >= 0) {
            return getAttribute(ind);
        } else {
            return getAttribute(attrNames.indexOf(name)+ATTRNAMES.size());
        }
    }

    @Override
    public Class getAttributeType(int ind) {
        if(ind == 1) {
            return String.class;
        } else {
            return Double.class;
        }
    }

    @Override
    public List<String> getAttributeNames() {
        ArrayList<String> names = new ArrayList<>(ATTRNAMES);
        names.addAll(attrNames);
        return names;
    }

    @Override
    public List<Object> getAttributes() {
        ArrayList<Object> list = new ArrayList<>(getAttributeNames().size());
        for(int i = 0; i < getAttributeNames().size(); i++) {
            list.add(getAttribute(i));
        }
        return list;
    }

    @Override
    public synchronized Geometry getGeometry() {
        if(geom == null) {
            Shape shp = getTransform().createTransformedShape(getShape());
            GeometryFactory factory = new GeometryFactory();
            geom = factory.createPolygon((LinearRing)JTS.toGeometry(shp, factory), new LinearRing[0]);
        }
        return geom;
    }

    @Override
    public Object getId() {
        if(getParent() == null) {
            return "0";
        }

        return getParent().getId() + "-" + indIfs;
    }

    @Override
    public Class getIdType() {
        return String.class;
    }

    
    public static int getLevel(String code) {
        return code.lastIndexOf('0') + 2;
    }
    
    public class RuralFeature extends AbstractSimpleFeature {

        public void setAttribute(String name, double val) {
            setRuralAttribute(name, val);
        }
        
        @Override
        public Object getAttribute(int ind) {
            switch(ind) {
                case 0:
                    return getRuralCode();
                case 1:
                    return getLevel();
                case 2:
                    return getRuralGeometry().getArea();
                case 3:
                    return getRuralHousing();
                case 4:
                    return getRuralHousing() / (getRuralRealArea() / 10000);  
                case 5:
                    return getRuralHousingUser();
                case 6:
                    return getRuralHousingUser() / (getRuralRealArea() / 10000);
                case 7:
                    return getRuralHousingUser() - getRuralHousing();
                case 8:
                    return 100*(getRuralHousingUser() - getRuralHousing()) / (getRuralHousing()+1);
                default:
                    return ruralAttrValues.get(ind-RURAL_ATTRNAMES.size());
            }
        }
        
        @Override
        public Object getAttribute(String name) {
            int ind = RURAL_ATTRNAMES.indexOf(name);
            if(ind >= 0) {
                return getAttribute(ind);
            } else {
                return getAttribute(ruralAttrNames.indexOf(name)+RURAL_ATTRNAMES.size());
            }
        }

        @Override
        public List<String> getAttributeNames() {
            ArrayList<String> names = new ArrayList<>(RURAL_ATTRNAMES);
            names.addAll(ruralAttrNames != null ? ruralAttrNames : Collections.EMPTY_LIST);
            return names;
        }

        @Override
        public Class getAttributeType(int ind) {
            if(ind == 1) {
                return String.class;
            } else {
                return Double.class;
            }
        }
    
        @Override
        public Geometry getGeometry() {
            return getRuralGeometry();
        }

        @Override
        public Object getId() {
            return FractalElem.this.getId();
        }
    }
}
