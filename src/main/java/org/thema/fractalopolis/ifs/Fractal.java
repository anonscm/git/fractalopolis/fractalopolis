/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis.ifs;

import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.thema.common.ProgressBar;
import org.thema.fractalopolis.HousingModel;
import org.thema.fractalopolis.evaluator.Evaluator;
import org.thema.fractalopolis.ifs.generator.FractalGenerator;
import org.thema.fractalopolis.ifs.generator.LargeScaleFractalGenerator;
import org.thema.parallel.AbstractParallelTask;
import org.thema.parallel.ExecutorService;

/**
 *
 * @author Gilles Vuidel
 */
public class Fractal {
    
    public enum Scale{ MACRO, MICRO }
    
    private Ifs ifs;
    protected FractalGenerator fracGen;
    private FractalModel fractalModel;
    private AffineTransform initTransform;
    
    private HousingModel userModel;
    
    private double userPop = 0;
    
    private Scale scale;
    
    private List<Evaluator> evaluators;
    
    private boolean autoUpdate = false;
    
    private transient FractalGroupLayer fracLayer;
    private transient ProgressBar progressBar;
    
    /** Creates a new instance of Fractal */
    public Fractal(Ifs i, AffineTransform initTransform, Scale scale, String startCode, List<Evaluator> evaluators) {
        ifs = i;
        this.scale = scale;
        this.evaluators = evaluators;
        this.initTransform = initTransform;
        this.fracGen = new LargeScaleFractalGenerator(ifs);
        fractalModel = new FractalModel(fracGen.createRootElem(initTransform, startCode));
        userModel = new HousingModel(ifs);
    }
    
    public Fractal(Fractal f, List<Evaluator> evals) {
        this.ifs = f.ifs;
        this.scale = f.scale;
        this.evaluators = evals;
        this.initTransform = f.initTransform;
        this.fracGen = f.fracGen;
        this.fractalModel = f.fractalModel;
        this.userModel = f.userModel;
    }
    
    public void reset() {
        fractalModel.init(fracGen.createRootElem(initTransform, fractalModel.getRootElem().getCode()));
        getFractalGroupLayer().setIteration(0);
        userModel = new HousingModel(ifs);
    }
    
    public void generate(int nbIter) {
        if(fractalModel.getNbIteration() == 0) {
            userModel = new HousingModel(ifs);
        }
        for(int i = 0; i < nbIter; i++) {
            fractalModel.addIteration(fracGen.iterate(fractalModel.getLastIteration()));
            userModel.addStep(fractalModel.getNbIteration());
        }
    }

    public Scale getScale() {
        return scale;
    }

    public List<Evaluator> getEvaluators() {
        return evaluators;
    }
    
    public Ifs getIfs() { 
        return ifs; 
    }
    
    public FractalModel getFractalModel() { 
        return fractalModel; 
    }

    public synchronized FractalGroupLayer getFractalGroupLayer() {
        if(fracLayer == null) {
            fracLayer = new FractalGroupLayer(this, "Fractal");
        }
        return fracLayer; 
    }
    
    public FractalLayer getFractalLayer() { 
        return getFractalGroupLayer().getFractalLayer();
    }

    public void setProgressBar(ProgressBar progressBar) {
        this.progressBar = progressBar;
    }

    public boolean isAutoUpdate() {
        return autoUpdate;
    }

    public void setAutoUpdate(boolean autoUpdate) {
        this.autoUpdate = autoUpdate;
    }
    
    public void forceUpdate() {
        updateAll(true);
    }
    
    
    public void updateAll(boolean force) {
        updateAll(fractalModel.getRootElem(), force);
    }
    
    public void updateAll(FractalElem elem, boolean force) {
        if(autoUpdate || force) {
            progressBar.reset();
            progressBar.setNote("Update...");
            updateEvals(elem, progressBar);
            progressBar.close();
        }
        updateUserStat(elem);
    }

    public void updateEval(Evaluator eval, boolean force) {
        if(autoUpdate || force) {
            progressBar.reset();
            progressBar.setNote("Eval...");
            updateEval(eval, fractalModel.getRootElem(), progressBar);
            progressBar.close();
        }
    }
            
    public HousingModel getRegModel() {
        return new HousingModel(this);
    }

    public HousingModel getUserModel() {
        return userModel;
    }
    
    public double getUserPop() {
        return userPop;
    }

    public void setUserPop(double userPop) {
        this.userPop = userPop;
        updateUserStat(fractalModel.getRootElem());
    }
    
    // calculate user for elem and all children
    public void updateUserStat(FractalElem elem) {
        if(elem.getParent() == null) {
            elem.setUser(userPop > 0 ? userPop : elem.getHousing());
        } else {
            elem.setUser(elem.getParent().getUser() * userModel.getCoef(elem.getStep(), ifs.getIndCoef(elem.getIndIfs())));
        }
        
        for(FractalElem e : elem.getChildren()) {
            updateUserStat(e);
        }
    }
    
    private void updateEvals(FractalElem elem, ProgressBar mon) {
        List<FractalElem> elems = elem.getAllChildren();
        if(elem.getParent() != null) { // for rural eval
            elems.add(elem.getParent());
        }
        List<Evaluator> evals = new ArrayList<>();
        for(Evaluator eval : evaluators) {
            if(eval.isEnabled()) {
                evals.add(eval);
            }
        }
        EvalTask task = new EvalTask(evals, elems, mon);
        ExecutorService.execute(task);
    }
    
    private void updateEval(Evaluator eval, FractalElem elem, ProgressBar mon) {
        List<FractalElem> elems = elem.getAllChildren();
        if(elem.getParent() != null) { // for rural eval
            elems.add(elem.getParent());
        }
        EvalTask task = new EvalTask(Collections.singletonList(eval), elems, mon);
        ExecutorService.execute(task);
    }
    
    
    private class EvalTask extends AbstractParallelTask<Void, Void> {

        private List<Evaluator> evals;
        private List<FractalElem> elems;

        public EvalTask(List<Evaluator> evals, List<FractalElem> elems, ProgressBar mon) {
            super(mon);
            this.evals = evals;
            this.elems = elems;
        }

        @Override
        public int getSplitRange() {
            return evals.size()*elems.size();
        }

        @Override
        public Void execute(int start, int end) {
            for(int i = start; i < end; i++) {
                Evaluator eval = evals.get(i / elems.size());
                FractalElem elem = elems.get(i % elems.size());
                eval.updateEval(elem);
                incProgress(1);
            }
            return null;
        }

        @Override
        public void gather(Void results) {
        }

        @Override
        public Void getResult() {
            return null;
        }
        
    }
}
