/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis.ifs;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.util.List;
import org.thema.drawshape.RectModShape;
import org.thema.drawshape.event.ShapeEvent;
import org.thema.drawshape.event.ShapeListener;
import org.thema.drawshape.feature.FeatureBasedShape;
import org.thema.drawshape.style.SimpleStyle;

/**
 *
 * @author Gilles Vuidel
 */
public class FractalElemShape extends RectModShape implements FeatureBasedShape, ShapeListener {

    private FractalElem elem;
    private FractalLayer layer;
    
    static {
        DEFAULT_SELECTSTYLE = new SimpleStyle(new Color(0, 0, 0, 150), 3);
    }
    
    public FractalElemShape(FractalElem elem, FractalLayer layer) {
        super(elem.getShape(), new AffineTransform(elem.getTransform()));
        style = layer.getStyle();
        this.elem = elem;
        this.layer = layer;
        setLimitShape(getLimitShape());
        addShapeListener(this);
    }

    @Override
    public void setSelection(boolean isSelected) {
        if(isSelected) {
            setLimitShape(getLimitShape());
        }
        super.setSelection(isSelected);
    }
    
    public void updateLimit() {
        setLimitShape(getLimitShape());
    }
    
    protected Shape getLimitShape() {
        if(elem.getParent() == null || (!layer.isVerifyOverlap() && !layer.isVerifyParentLimit())) {
            return null;
        }
        Area parent = new Area(elem.getParent().getTransform().createTransformedShape(elem.getParent().getShape()));

        if(layer.isVerifyOverlap()) {
            List<FractalElem> lstBShape = elem.getBrothers();
            for (FractalElem lstBShape1 : lstBShape) {
                parent.subtract(new Area(lstBShape1.getTransform().createTransformedShape(lstBShape1.getShape())));
            }
        }
        GeneralPath p = new GeneralPath();
        p.append(parent.getPathIterator(null), false);
        return p;
    }
    
    @Override
    public void drawSelection(Graphics2D g, AffineTransform t) {
        super.drawSelection(g, t);
        // draw a cross
        Point2D c = getCentre();
        t.transform(c, c);
        g.setColor(Color.RED.darker());
        g.setStroke(new BasicStroke(1));
        g.drawLine((int)c.getX()-10, (int)c.getY(), (int)c.getX()+10, (int)c.getY());
        g.drawLine((int)c.getX(), (int)c.getY()-10, (int)c.getX(), (int)c.getY()+10);
    }

    @Override
    public FractalElem getFeature() {
        return elem;
    }
    
    @Override
    public void shapeChanged(ShapeEvent e) {
        elem.setTransform(getTransform());
        layer.shapeChanged(this);
    }

    @Override
    public void setShape(Shape shape) {
        super.setShape(shape); 
        elem.setShape(shape);
    }
   
}
