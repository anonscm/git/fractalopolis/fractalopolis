/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis.ifs;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.GeneralPath;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import org.thema.drawshape.DrawableShape;
import org.thema.drawshape.RectModShape;
import org.thema.drawshape.event.SelectionListener;
import org.thema.drawshape.event.ShapeEvent;
import org.thema.drawshape.event.ShapeListener;
import org.thema.drawshape.layer.AbstractStyledLayer;
import org.thema.drawshape.style.Style;
import static org.thema.fractalopolis.ifs.Ifs.style;

/**
 *
 * @author Gilles Vuidel
 */
public class IfsLayer extends AbstractStyledLayer implements ShapeListener, SelectionListener{

    private Ifs ifs;
    private transient List<RectModShape> shapes;
    private transient boolean verifyOverlap;
    private transient boolean verifyParentLimit;
    
    public IfsLayer(Ifs ifs, Style style) {
        super("IFS", style);
        this.ifs = ifs;
        shapes = new ArrayList<>();
        for (int i = 0; i < ifs.getNbTransform(); i++) {
            shapes.add(createShape(ifs.getTransform(i)));
        }
    }

    public void setTransform(AffineTransform t, int ind) {
        shapes.get(ind).setTransform(t);
        fireStyleChanged();
    }
    
    public void addShape(AffineTransform t) {
        shapes.add(createShape(t));
        fireVisibilityChanged();
    }

    public void removeShape(int ind) {
        RectModShape sh = shapes.remove(ind);
        sh.removeShapeListener(this);
        sh.removeSelectionListener(this);
        fireVisibilityChanged();
    }
    
    @Override
    public List<? extends DrawableShape> getDrawableShapes() {
        return shapes;
    }
    
    private RectModShape createShape(AffineTransform t) {
        RectModShape shape = new RectModShape(ifs.getInitShape(), t);
        shape.setStyle(style);
        shape.addShapeListener(this);
        shape.addSelectionListener(this);
        return shape;
    }
    
    private Shape generateLimitShape(RectModShape shape) {
        Area parent = new Area(ifs.getInitShape());
        if(verifyOverlap) {
            for (int i = 0; i < ifs.getNbTransform(); i++) {
                if (shapes.get(i) != shape) {
                    parent.subtract(new Area(ifs.getTransform(i).createTransformedShape(ifs.getInitShape())));
                }
            }
        }
        GeneralPath p = new GeneralPath();
        p.append(parent.getPathIterator(new AffineTransform()), false);
        return p;
    }
    
    public void setVerifyOverlap(boolean b) { 
        verifyOverlap = b; 
        if(verifyOverlap) {
            setVerifyParentLimit(true);
        }
    }
    
    public void setVerifyParentLimit(boolean b) { 
        verifyParentLimit = b; 
        if(!verifyParentLimit) {
            setVerifyOverlap(false);
        }
    }
    
    public boolean getVerifyOverlap() { 
        return verifyOverlap; 
    }
    public boolean getVerifyParentLimit() { 
        return verifyParentLimit; 
    }
    
    @Override
    public void selectionChanged(EventObject e) {
        RectModShape shape = (RectModShape)e.getSource();
        if(shape.isSelected()) {
            if(verifyParentLimit || verifyOverlap) {
                shape.setLimitShape(generateLimitShape(shape));
            } else {
                shape.setLimitShape(null);
            }
        }
    }
    
    @Override
    public void shapeChanged(ShapeEvent e) {
        ifs.getTableModel().fireTableDataChanged();
    }

    public void updateShapes() {
        shapes = new ArrayList<>();
        for (int i = 0; i < ifs.getNbTransform(); i++) {
            shapes.add(createShape(ifs.getTransform(i)));
        }
        fireVisibilityChanged();
    }

}
