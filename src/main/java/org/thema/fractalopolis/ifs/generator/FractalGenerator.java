/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis.ifs.generator;

import java.awt.geom.AffineTransform;
import java.util.List;
import org.thema.fractalopolis.ifs.FractalElem;

/**
 *
 * @author Gilles Vuidel
 */
public interface FractalGenerator {
    
    public FractalElem createRootElem(AffineTransform trans, String startCode);
    
    public List<FractalElem> iterate(List<FractalElem> lst);

}
