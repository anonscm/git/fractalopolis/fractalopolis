/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis.ifs.generator;

import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.List;
import org.thema.fractalopolis.ifs.FractalElem;
import org.thema.fractalopolis.ifs.Ifs;

/**
 *
 * @author Gilles Vuidel
 */
public class LargeScaleFractalGenerator implements FractalGenerator {
    
    protected Ifs ifs;
    
    /** Creates a new instance of LargeScaleFractalGenerator */
    public LargeScaleFractalGenerator(Ifs i) {
        ifs = i;
    }

    @Override
    public List<FractalElem> iterate(List<FractalElem> lstFrac) {
        List<FractalElem> lstRes = new ArrayList<>();
        
        for(FractalElem elem : lstFrac) {
            List<FractalElem> children = new ArrayList<>();
            for(int j = 0; j < ifs.getNbTransform(); j++)
            {
                AffineTransform t = new AffineTransform(elem.getTransform());
                t.concatenate(ifs.getTransform(j));
                FractalElem e = new FractalElem(ifs, j, t);
                lstRes.add(e);
                children.add(e);
            }
            elem.setChildren(children);
        }
        
        return lstRes;
    }

    @Override
    public FractalElem createRootElem(AffineTransform trans, String startCode) {
        return new FractalElem(ifs, -1, trans, startCode);
    }

  
    
}
