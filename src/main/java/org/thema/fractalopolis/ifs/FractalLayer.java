/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis.ifs;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import org.thema.common.Util;
import org.thema.data.IOFeature;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.FeatureGetter;
import org.thema.drawshape.layer.AbstractStyledLayer;
import org.thema.drawshape.layer.FeatureLayer;
import org.thema.drawshape.style.FeatureStyle;
import org.thema.drawshape.style.table.ColorBuilder;
import org.thema.drawshape.style.table.ColorRamp;
import org.thema.drawshape.style.table.FeatureAttributeIterator;
import org.thema.drawshape.style.table.StrokeRamp;
import org.thema.drawshape.style.table.UniqueColorTable;
import org.thema.fractalopolis.App;
import org.thema.fractalopolis.access.Accessibilities;
import org.thema.fractalopolis.access.Accessibility;
import org.thema.fractalopolis.evaluator.Evaluator;

/**
 *
 * @author gib
 */
public class FractalLayer extends AbstractStyledLayer implements FeatureGetter {

    private Fractal fractal;
    private int iteration;
    private List<FractalElemShape> shapes;
    
    private boolean verifyOverlap, verifyParentLimit;

    public FractalLayer(Fractal fractal, String name) {
        super(name, new FeatureStyle(new Color(0, 0, 255, 127), new Color(0, 0, 0, 127)));
        this.fractal = fractal;
        shapes = new ArrayList<>();
        setIteration(0);
    }
    
    public final void setIteration(int iter) {
        List<FractalElem> elems = fractal.getFractalModel().getIteration(iter);
        shapes.clear();
        for(FractalElem elem : elems) {
            shapes.add(new FractalElemShape(elem, this));
        }
        
        iteration = iter;
        
        setView(getStyle().getAttrFill(), getStyle().getAttrContour() != null);

        fireVisibilityChanged();
    }
    
    public FeatureStyle setView(String attr, boolean landPot) {
        FeatureStyle style;
        if(attr == null) {
            style = new FeatureStyle(new Color(0, 0, 255, 127), Color.BLACK);
        } else {
            ColorBuilder ramp;
            if(attr.equals("level")) {
                ramp = new UniqueColorTable(Arrays.asList(1, 2, 3, 4), Arrays.asList(ColorRamp.setTransparency(new Color[]{ColorRamp.RAMP_BLUE[4], ColorRamp.RAMP_BLUE[3], ColorRamp.RAMP_BLUE[1], ColorRamp.RAMP_BLUE[0]}, 220)));
            } else if(attr.contains("%") || attr.contains("rate_")) {
                ramp = new ColorRamp(ColorRamp.setTransparency(ColorRamp.RAMP_SYM_BLUE_RED, 220), -100, 0, 100);
            } else if(attr.contains("-") || attr.contains("diff_")) {
                ramp = new ColorRamp(ColorRamp.setTransparency(ColorRamp.RAMP_SYM_BLUE_RED, 220), -1, 0, 1);
                ((ColorRamp)ramp).setBounds(new FeatureAttributeIterator<>(getFeatures(), attr));
            } else if(attr.equals("suitability")) {
                ramp = new ColorRamp(ColorRamp.setTransparency(ColorRamp.reverse(ColorRamp.RAMP_SYM_GREEN_RED), 220), 0, 0.5, 1);
            } else if(isAccess(attr)) {
                ramp = new ColorRamp(ColorRamp.setTransparency(ColorRamp.reverse(ColorRamp.RAMP_SYM_GREEN_RED), 220), 0, 1);
            } else {
                ramp = new ColorRamp(ColorRamp.setTransparency(ColorRamp.RAMP_INVGRAY, 220),
                        new FeatureAttributeIterator<>(getFeatures(), attr));
            }
            
            style = new FeatureStyle(attr, ramp, null, new ColorRamp(new Color[]{Color.BLACK}));
        }
        if(landPot) {
            style = new FeatureStyle(style.getAttrFill(), style.getRampFill(), "building_land_potential", new ColorRamp(ColorRamp.reverse(ColorRamp.RAMP_BROWN)), "building_land_potential", new StrokeRamp(5, 0.5f));
        } 
        
        setStyle(style);
        return style;
    }
    
    @Override
    public List<FractalElemShape> getDrawableShapes() {
        return shapes;
    }

    public boolean isVerifyOverlap() {
        return verifyOverlap;
    }

    public void setVerifyOverlap(boolean verifyOverlap) {
        this.verifyOverlap = verifyOverlap;
        for(FractalElemShape shp : getDrawableShapes()) {
            if (shp.isSelected()) {
                shp.updateLimit();
            }
        }
        fireStyleChanged();
    }

    public boolean isVerifyParentLimit() {
        return verifyParentLimit;
    }

    public void setVerifyParentLimit(boolean verifyParentLimit) {
        this.verifyParentLimit = verifyParentLimit;
        for(FractalElemShape shp : getDrawableShapes()) {
            if (shp.isSelected()) {
                shp.updateLimit();
            }
        }
        fireStyleChanged();
    }

    @Override
    public FeatureStyle getStyle() {
        return (FeatureStyle) super.getStyle();
    }
    
    @Override
    public List<FractalElem> getFeatures() {
        return fractal.getFractalModel().getIteration(iteration);
    }

    
    @Override
    public JPopupMenu getContextMenu() {
        JPopupMenu menu = super.getContextMenu();

        menu.add(new JMenuItem(new AbstractAction(java.util.ResourceBundle.getBundle("org/thema/drawshape/ui/Bundle").getString("EXPORT...")) {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                final File file = Util.getFileSave(".gpkg|.shp");
                if(file == null) {
                    return;
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            IOFeature.saveFeatures(getFeatures(), file);
                        } catch(IOException e) {
                            Logger.getLogger(FeatureLayer.class.getName()).log(Level.SEVERE, null, e);
                            JOptionPane.showMessageDialog(null, "Erreur pendant l'export :\n" + e.getLocalizedMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }).start();
            }
        }));
        return menu;
    }
                       
    void shapeChanged(FractalElemShape shape) {
        fractal.updateAll(shape.getFeature(), false);
        if(fractal.isAutoUpdate()) {
            App.mainFrame.refreshStat();
        }
       
    }
    
    private boolean isAccess(String attrName) {
        for(Evaluator eval : fractal.getEvaluators()) {
            if (eval instanceof Accessibilities) {
                for(Accessibility access : ((Accessibilities)eval).getAccessibilities()) {
                    if(access.getShortName().equals(attrName)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
}
