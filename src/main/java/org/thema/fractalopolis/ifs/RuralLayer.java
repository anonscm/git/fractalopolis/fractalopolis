/*
 * Copyright (C) 2021 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.fractalopolis.ifs;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import org.thema.common.Util;
import org.thema.data.IOFeature;
import org.thema.data.feature.FeatureGetter;
import org.thema.drawshape.layer.AbstractStyledLayer;
import org.thema.drawshape.layer.FeatureLayer;
import org.thema.drawshape.style.FeatureStyle;
import org.thema.drawshape.style.table.UniqueColorTable;

/**
 *
 * @author gvuidel
 */
public class RuralLayer extends AbstractStyledLayer implements FeatureGetter {

    private Fractal fractal;
    private int iteration;
    private List<RuralElemShape> shapes;
    

    public RuralLayer(Fractal fractal, String name) {
        super(name, DEFAULT_STYLE);
        this.fractal = fractal;
        shapes = new ArrayList<>();
        setIteration(0);
    }
    
    public final void setIteration(int iter) {
        shapes.clear();
        for(int i = 0; i < iter; i++) {
            for(FractalElem elem : fractal.getFractalModel().getIteration(i)) {
                shapes.add(new RuralElemShape(elem.getRuralFeature()));
            }
        }
        
        iteration = iter;
        
        setView(getStyle().getAttrFill(), getStyle());

        fireVisibilityChanged();
    }
    
    public void setView(String attr, FeatureStyle style) {
        if(attr == null || "code".equals(attr)) {
            style = DEFAULT_STYLE;
        } else {
            List<String> attrNames = fractal.getFractalModel().getIteration(0).get(0).getRuralFeature().getAttributeNames();
            if(!attrNames.contains(attr)) {
                style = DEFAULT_STYLE;
            } 
        }
        setStyle(style);
    }
    
    @Override
    public List<RuralElemShape> getDrawableShapes() {
        return shapes;
    }

    @Override
    public FeatureStyle getStyle() {
        return (FeatureStyle) super.getStyle();
    }
    
    @Override
    public Collection getFeatures() {
        List<FractalElem.RuralFeature> features = new ArrayList<>();
        for(int i = 0; i < iteration; i++) {
            for(FractalElem elem : fractal.getFractalModel().getIteration(i)) {
                features.add(elem.getRuralFeature());
            }
        }
        return features;
    }

    @Override
    public JPopupMenu getContextMenu() {
        JPopupMenu menu = super.getContextMenu();

        menu.add(new JMenuItem(new AbstractAction(java.util.ResourceBundle.getBundle("org/thema/drawshape/ui/Bundle").getString("EXPORT...")) {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                final File file = Util.getFileSave(".gpkg|.shp");
                if(file == null) {
                    return;
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            IOFeature.saveFeatures(getFeatures(), file);
                        } catch(IOException e) {
                            Logger.getLogger(FeatureLayer.class.getName()).log(Level.SEVERE, null, e);
                            JOptionPane.showMessageDialog(null, "Erreur pendant l'export :\n" + e.getLocalizedMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }).start();
            }
        }));
        return menu;
    }
    
    public static final FeatureStyle DEFAULT_STYLE = new FeatureStyle("code", 
                new UniqueColorTable(Arrays.asList(1, 2, 3, 4, 5), 
                                     Arrays.asList(new Color[] {
                                        new Color(0,     104,	55, 127),
                                        new Color(49,	163,	84, 127),
                                        new Color(120,	198,	121, 127),
                                        new Color(194,	230,	153, 127),
                                         new Color(255,	255,	204, 127)})) {
            @Override
            public Color getColor(Object val) {
                if(val instanceof Number) {
                    return super.getColor(val);
                }
                return super.getColor(val.toString().length()); 
            }
                                         
    });
}
