/*
 * Copyright (C) 2021 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.fractalopolis.ifs;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

/**
 * AffineTransform converter for XStream Library
 * @author gvuidel
 */
public class AffineTransformConverter implements Converter {

    @Override
    public void marshal(Object value, HierarchicalStreamWriter writer, MarshallingContext mc) {
        AffineTransform trans = (AffineTransform) value;
        writer.startNode("Transform");
        writer.startNode("default");
        writer.startNode("m00");
        writer.setValue(Double.toString(trans.getScaleX()));
        writer.endNode();
        writer.startNode("m01");
        writer.setValue(Double.toString(trans.getShearX()));
        writer.endNode();
        writer.startNode("m02");
        writer.setValue(Double.toString(trans.getTranslateX()));
        writer.endNode();
        writer.startNode("m10");
        writer.setValue(Double.toString(trans.getShearY()));
        writer.endNode();
        writer.startNode("m11");
        writer.setValue(Double.toString(trans.getScaleY()));
        writer.endNode();
        writer.startNode("m12");
        writer.setValue(Double.toString(trans.getTranslateY()));
        writer.endNode();
        writer.endNode();
        writer.endNode();
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext uc) {
        reader.moveDown();reader.moveDown();
        reader.moveDown();
        double m00 = Double.parseDouble(reader.getValue());
        reader.moveUp();
        reader.moveDown();
        double m01 = Double.parseDouble(reader.getValue());
        reader.moveUp();
        reader.moveDown();
        double m02 = Double.parseDouble(reader.getValue());
        reader.moveUp();
        reader.moveDown();
        double m10 = Double.parseDouble(reader.getValue());
        reader.moveUp();
        reader.moveDown();
        double m11 = Double.parseDouble(reader.getValue());
        reader.moveUp();
        reader.moveDown();
        double m12 = Double.parseDouble(reader.getValue());
        reader.moveUp();
        reader.moveUp();reader.moveUp();
        return new AffineTransform(m00, m10, m01, m11, m02, m12);
    }

    @Override
    public boolean canConvert(Class type) {
        return AffineTransform.class.equals(type) ;
    }
    
}
