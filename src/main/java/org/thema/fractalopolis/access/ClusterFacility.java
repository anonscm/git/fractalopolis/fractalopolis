/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.fractalopolis.access;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.MultiPoint;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.thema.common.parallel.BufferForkJoinTask;
import org.thema.data.feature.AbstractFeature;

/**
 *
 * @author Gilles Vuidel
 */
public class ClusterFacility extends AbstractFeature {

    private static final String [] ATTR_NAMES = {"Nb", "Nb_Type"};

    private Object id;
    private List<Facility> facilities;
    private Polygon geom;
    private int nb = 0;
    private int nbType = 0;
    
    
    /** Creates a new instance of ClusterService */
    protected ClusterFacility(Object id, Polygon poly) {
        this.id = id;
        facilities = new ArrayList<>();
        geom = poly;
    }
    
    public int getNbFacilities() {
        return facilities.size();
    }
    public int getNbTypeFacilities() {
        return nbType;
    }
    
    public List<Facility> getFacilities() {
        return facilities;
    }
    
    protected void addFacility(Facility s) {
        boolean typeExist = false;
        for (Facility serv : facilities) {
            if(serv.getType().equals(s.getType())) {
                typeExist = true;
                break;
            }
        }
        if(!typeExist) {
            nbType++;

        }
            
        nb++;

        facilities.add(s);
        s.setCluster(this);
    }

    @Override
    public Object getId() {
        return id;
    }
    
    @Override
    public Polygon getGeometry() {
        return geom;
    }
    
    public MultiPoint getFacilityGeometry() {
        Point[] points = new Point[facilities.size()];
        for(int i = 0; i < points.length; i++) {
            points[i] = facilities.get(i).getGeometry();
        }
        return new GeometryFactory().createMultiPoint(points);
    }
   
    
    public static List<ClusterFacility> createClusters(Collection<Facility> services, double dist) {
        if(services.isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        ArrayList<ClusterFacility> lstClust = new ArrayList<>();
        Point[] tabPoint = new Point[services.size()];
        int i = 0;
        for(Facility s : services) {
            tabPoint[i++] = s.getGeometry();
        }
        MultiPoint serv = new MultiPoint(tabPoint, tabPoint[0].getFactory());
        Geometry servBuf = BufferForkJoinTask.buffer(serv, dist/2);
        for(i = 0; i < servBuf.getNumGeometries(); i++) {
            ClusterFacility clust = new ClusterFacility(i+1, (Polygon)servBuf.getGeometryN(i));
            for(Facility s : services) {
                if (clust.geom.contains(s.getGeometry())) {
                    clust.addFacility(s);
                }
            }
            lstClust.add(clust);
        }
            
        return lstClust;
    }

    @Override
    public Object getAttribute(int ind) {
        return getAttribute(ATTR_NAMES[ind]);
    }

    @Override
    public Object getAttribute(String name) {
        if(name.equals(ATTR_NAMES[0])) {
            return nb;
        } else {
            return nbType;
        }

    }

    @Override
    public Class getAttributeType(int ind) {
        return Double.class;
    }

    @Override
    public List<String> getAttributeNames() {
        return Arrays.asList(ATTR_NAMES);
    }

    @Override
    public List<Object> getAttributes() {
        return Arrays.asList(new Object[]{nb, nbType});
    }

    @Override
    public Class getIdType() {
        return Integer.class;
    }
}
