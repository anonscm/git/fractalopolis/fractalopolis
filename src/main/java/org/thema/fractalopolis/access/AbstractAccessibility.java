/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis.access;

import org.thema.fractalopolis.Project;
import org.thema.fractalopolis.ifs.FractalElem;

/**
 *
 * @author gvuidel
 */
public abstract class AbstractAccessibility implements Accessibility {

    private Project project;
    private double coef = 1;
    private boolean enabled = true;

    public AbstractAccessibility(Project project) {
        this.project = project;
    }

    protected Project getProject() {
        return project;
    }
    
    @Override
    public boolean isUsable() {
        return true;
    }

    public double getCoef() {
        return coef;
    }

    public void setCoef(double coef) {
        this.coef = coef;
    }

    @Override
    public boolean isEnabled() {
        return isUsable() && enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
    

    @Override
    public void updateEval(FractalElem elem) {
        elem.setAttribute(getShortName(), calcAccess(elem, TypeZone.Urban));
        elem.setRuralAttribute(getShortName(), calcAccess(elem, TypeZone.Rural));
    }

    @Override
    public double getEval(FractalElem elem, TypeZone zone) {
        if(zone == TypeZone.Urban) {
            return (Double)elem.getAttribute(getShortName());
        } else {
            if(elem.getRuralFeature() != null) {
                return (Double)elem.getRuralFeature().getAttribute(getShortName());
            } else {
                return 0.0;
            }
        }
    }


    @Override
    public String toString() {
        return getName();
    }
   
    
}
