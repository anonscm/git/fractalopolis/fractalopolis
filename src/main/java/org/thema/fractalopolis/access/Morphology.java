/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis.access;

import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.math.DoubleRange;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.DefaultFeatureCoverage;
import org.thema.fractalopolis.Project;
import org.thema.fractalopolis.ifs.Fractal.Scale;
import org.thema.fractalopolis.ifs.FractalElem;

/**
 *
 * @author gvuidel
 */
public class Morphology extends AbstractAccessibility {

    private Scale scale;
    
    public Morphology(Project project, Scale scale) {
        super(project);
        this.scale = scale;
    }

    @Override
    public String getName() {
        return "Morphology";
    }

    @Override
    public String getShortName() {
        return "morph";
    }

    @Override
    public double calcAccess(FractalElem elem, TypeZone typeZone) {
        if(typeZone == TypeZone.Rural) {
            return 0.0;
        }
        if(elem.getParent() == null || elem.getRank() > 0) {
            return 1.0;
        }
        List<Envelope> squares = new ArrayList<>();
        for(FractalElem brother : elem.getBrothers()) { 
            if (brother.getRank() == 0) {
                squares.add(brother.getGeometry().getEnvelopeInternal());
            }
        }
        for(FractalElem parent : elem.getParent().getBrothers()) { 
            for (FractalElem cousin : parent.getChildren()) {
                if (cousin.getRank() == 0) {
                    squares.add(cousin.getGeometry().getEnvelopeInternal());
                }
            }
        }
                
        Envelope env = elem.getGeometry().getEnvelopeInternal();
        DoubleRange xRange = new DoubleRange(env.getMinX(), env.getMaxX());
        DoubleRange yRange = new DoubleRange(env.getMinY(), env.getMaxY());
        double minDist = elem.getSize();
        for(Envelope square : squares) {
            if(xRange.overlapsRange(new DoubleRange(square.getMinX(), square.getMaxX())) ||
               yRange.overlapsRange(new DoubleRange(square.getMinY(), square.getMaxY()))) {
                double d = env.distance(square);
                if(d < minDist) { 
                    minDist = d;
                }
                
            }
        }

        Envelope zone = new Envelope(env);
        zone.expandBy(elem.getSize());
        List<Geometry> geoms = new ArrayList<>();
        for(FractalElem brother : elem.getBrothers()) {
            if (zone.intersects(brother.getGeometry().getEnvelopeInternal())) {
                geoms.add(brother.getGeometry());
            }
        } 
        for(FractalElem parent : elem.getParent().getBrothers()) {
            for (FractalElem cousin : parent.getChildren()) {
                if (zone.intersects(cousin.getGeometry().getEnvelopeInternal())) {
                    geoms.add(cousin.getGeometry());
                }
            }
        }
        
        Geometry ring = new GeometryFactory().toGeometry(zone).difference(new GeometryFactory().buildGeometry(geoms).union());
        List<DefaultFeature> builds;
        if(scale == Scale.MACRO) {
            builds = getProject().getCoverage(Project.Layers.BUILD.toString()).getFeaturesIn(ring);
        } else {
            builds = getProject().getCoverage(Project.Layers.MICRO_BUILD.toString()).getFeaturesIn(ring);
        }
        DefaultFeatureCoverage<DefaultFeature> buildCov = new DefaultFeatureCoverage<>(builds);
        List<DefaultFeature> nearest = buildCov.getNearestFeatures(elem.getGeometry());
        if(!nearest.isEmpty()) {
            double d = nearest.get(0).getGeometry().distance(elem.getGeometry());
            if(d < minDist) {
                minDist = d;
            }
        }
        
        return minDist / elem.getSize();
    }

    @Override
    public int getFreq() {
        return -1; // works for all level
    }
    
    @Override
    public Type getType() {
        return Type.Morphology;
    }  

    @Override
    public Map<String, Object> getParams() {
        return Collections.EMPTY_MAP;
    }

    @Override
    public void setParams(Map<String, Object> params) {
        // No parameters to set
    }
    
}
