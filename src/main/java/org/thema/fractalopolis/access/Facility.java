/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis.access;

import org.locationtech.jts.geom.Point;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.thema.data.feature.AbstractFeature;
import org.thema.data.feature.Feature;

/**
 *
 * @author Gilles Vuidel
 */
public class Facility extends AbstractFeature {

    public static final String FREQ_FIELD = "freq";
    public static final String TYPE_FIELD = "type";

    private static final String [] ATTR_NAMES = {"Type", "Freq", "Cluster"};

    private Object id;
    private Object type;
    private int freq;    
    private Point geom;
    private ClusterFacility cluster;
    
    /** Creates a new instance of Service */
    public Facility(Object id, String type, int freq, Point geom) {
        this.id = id;
        this.type = type;
        this.freq = freq;
        this.geom = geom;
    }

    public int getFreq() {
        return freq;
    }
    
    @Override
    public Point getGeometry() { 
        return geom; 
    }
    
    public Object getType() { 
        return type; 
    }
    
    public void setCluster(ClusterFacility clust) {
        cluster = clust;
    }
    
    public static Map<Object, Facility> importFacilities(List<? extends Feature> features) {
        HashMap<Object, Facility> lst = new HashMap<>();
        int i = 1;
        for(Feature f : features) {
            Object id = "serv_" + i++;
            lst.put(id, new Facility(id, f.getAttribute(TYPE_FIELD).toString(),
                    ((Number)f.getAttribute(FREQ_FIELD)).intValue(),
                    (Point)f.getGeometry()));
        }
        
        return lst;
    }

    public ClusterFacility getCluster() {
        return cluster;
    }

    @Override
    public Object getAttribute(int ind) {
        return getAttribute(ATTR_NAMES[ind]);
    }

    @Override
    public Object getAttribute(String name) {
        if(name.equals(ATTR_NAMES[0])) {
            return type;
        } else if(name.equals(ATTR_NAMES[1])) {
            return freq;
        } else {
            return cluster.getId();
        }
    }

    @Override
    public Class getAttributeType(int ind) {
        if(ind == 0) {
            return String.class;
        }
        return Integer.class;
    }

    @Override
    public List<String> getAttributeNames() {
        return Arrays.asList(ATTR_NAMES);
    }

    @Override
    public List<Object> getAttributes() {
        return Arrays.asList(new Object[]{type, freq, cluster.getId()});
    }

    @Override
    public Object getId() {
        return id;
    }

    @Override
    public Class getIdType() {
        return Integer.class;
    }
}
