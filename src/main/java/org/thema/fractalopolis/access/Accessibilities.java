/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis.access;

import org.locationtech.jts.geom.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import org.thema.common.collection.HashMap2D;
import org.thema.common.fuzzy.DiscreteFunction;
import org.thema.fractalopolis.Project;
import org.thema.fractalopolis.Project.Layers;
import org.thema.fractalopolis.access.Accessibility.Type;
import org.thema.fractalopolis.access.Accessibility.TypeZone;
import org.thema.fractalopolis.evaluator.Evaluator;
import org.thema.fractalopolis.ifs.Fractal.Scale;
import org.thema.fractalopolis.ifs.FractalElem;
import org.thema.graph.pathfinder.DijkstraPathFinder;

/**
 *
 * @author gvuidel
 */
public class Accessibilities implements Evaluator {
    
    public static final int MAX_LEVEL = 4;
    
    public static final int DIST_EUCLID = 0;
    public static final int DIST_ROAD = 1;

    private List<Accessibility> accessibilities;
    private Scale scale;
    private Aggregator aggregator;
    private int distType = DIST_EUCLID;
    private int version;
    
    public Accessibilities(Project project, Scale scale) {
        this.scale = scale;
        setVersion(0, project);
    }

    public double evalAccess(FractalElem elem, Accessibility.TypeZone typeZone) {
        if(version == 2) {
            double eval = 0;
            double sumCoef = 0;
            for(Accessibility access : accessibilities) { 
                if(access.isEnabled()) {
                    double coef = aggregator.getCoef(access.getFreq(), access.getType());
                    eval += coef * access.getEval(elem, typeZone);
                    sumCoef += coef;
                }
            }
               
            return eval / sumCoef;  
        } else {
            int elemLevel = elem.getLevel() > MAX_LEVEL ? MAX_LEVEL : elem.getLevel();
            HashMap<Integer, Double> evals = new HashMap<>();
            for(int level = 1; level <= MAX_LEVEL; level++) {
                double eval = 0;
                double sumCoef = 0;
                for(Accessibility access : accessibilities) { 
                    if(access.isEnabled() && (access.getFreq() == level || access.getFreq() == -1)) {
                        double coef = aggregator.getCoef(elemLevel, level, access.getType());
                        eval += coef * access.getEval(elem, typeZone);
                        sumCoef += coef;
                    }
                }
                if(sumCoef > 0) {
                    evals.put(level, eval / sumCoef);
                }
            }
            double eval = 0;
            double sumCoef = 0;
            for(Integer level : evals.keySet()) {
                double coef = aggregator.getCoefAggreg(elemLevel, level);
                eval += coef * evals.get(level);
                sumCoef += coef;
            }
            return eval / sumCoef;            
        }
    }

    public List<Accessibility> getAccessibilities() {
        return accessibilities;
    }
    
    public TableModel getTableModel() {
        return new AccessTableModel();
    }

    public Aggregator getAggregator() {
        return aggregator;
    }

    public int getVersion() {
        return version;
    }
    
    public void setVersion(int version, Project project) {
        this.version = version;
        if(version == 2) {
            accessibilities = new ArrayList<>(Arrays.asList((Accessibility)
                    new AccessAll(project, scale, Type.Shop, 1, new DiscreteFunction(new double[]{0, 20000}, new double []{1, 0})), 
                    new AccessAll(project, scale, Type.Shop, 2, new DiscreteFunction(new double[]{3000, 6000}, new double []{1, 0})), 
                    new AccessAll(project, scale, Type.Shop, 3, new DiscreteFunction(new double[]{800, 1600}, new double []{1, 0})), 
                    new AccessAll(project, scale, Type.Shop, 4, new DiscreteFunction(new double[]{300, 600}, new double []{1, 0})), 
                    new AccessAll(project, scale, Type.Green, 1, new DiscreteFunction(new double[]{0, 20000}, new double []{1, 0})), 
                    new AccessAll(project, scale, Type.Green, 2, new DiscreteFunction(new double[]{1500, 3000}, new double []{1, 0})), 
                    new AccessAll(project, scale, Type.Green, 3, new DiscreteFunction(new double[]{500, 1000}, new double []{1, 0})), 
                    new AccessAll(project, scale, Type.Green, 4, new DiscreteFunction(new double[]{150, 300}, new double []{1, 0}))));
        } else {
            if(scale == Scale.MICRO) {
                accessibilities = new ArrayList<>(Arrays.asList((Accessibility)new AccessN1(project), new AccessN2(project), new AccessN34(project, 3), new AccessN34(project, 4),
                        new AccessMicroLeisure(project, 1), new AccessMicroLeisure(project, 2),
                        new AccessMicroLeisure(project, 3, new double[]{2000.0, 15000.0}, new double []{1.0, 0.0}), 
                        new AccessMicroLeisure(project, 4, new double[]{800.0, 1600.0}, new double []{1.0, 0.0}),
                        new Morphology(project, scale)));
            } else {
                accessibilities = new ArrayList<>(Arrays.asList((Accessibility)
                        new AccessMacroFacility12(project, 1), new AccessMacroFacility12(project, 2), new AccessMacroFacility34(project, 3), new AccessMacroFacility34(project, 4),
                        new AccessMacroLeisure(project, 1), new AccessMacroLeisure(project, 2), new AccessMacroLeisure(project, 3), new AccessMacroLeisure(project, 4),
                        new Morphology(project, scale)));
            }
            
        }
        aggregator = new Aggregator(version);
    }

    @Override
    public void updateEval(FractalElem elem) {
        for(Accessibility access : accessibilities) { 
            if(access.isEnabled()) {
                access.updateEval(elem);
            }
        }
        elem.setAttribute("suitability", evalAccess(elem, TypeZone.Urban));
        elem.setRuralAttribute("suitability", evalAccess(elem, TypeZone.Rural));
    }

    public Scale getScale() {
        return scale;
    }

    @Override
    public boolean isEnabled() {
        return !accessibilities.isEmpty();
    }

    public int getDistType() {
        return distType;
    }

    public void setDistType(int distType) {
        this.distType = distType;
    }
    
    public OriginDistance getOriginDistance(Point orig, double distMax) {
        if(distType == DIST_ROAD && Project.project.isLayerExist(Layers.MICRO_ROAD.toString())) {
            return new NetworkDistance(Project.project.getSpatialGraph(), DijkstraPathFinder.DIST_WEIGHTER, orig, distMax);
        } else {
            return new EuclideanDistance(orig);
        }
    }

    @Override
    public String toString() {
        return "Suitability";
    }
    
    
    private class AccessTableModel extends AbstractTableModel {
        private final String [] COLNAMES = new String[] {"Enabled", "Name"};
        
        @Override
        public int getRowCount() {
            return accessibilities.size();
        }

        @Override
        public int getColumnCount() {
            return COLNAMES.length;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Accessibility access = accessibilities.get(rowIndex);
            switch(columnIndex) {
                case 0: return access.isEnabled();
                case 1: return access.getName();
                default:  return null;
            }
        }

        @Override
        public String getColumnName(int columnIndex) {
            return COLNAMES[columnIndex];
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            if(columnIndex == 0) {
                return Boolean.class;
            }
            if(columnIndex == 1) {
                return String.class;
            }
            return null;
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            if(columnIndex > 0) {
                return false;
            }
            return accessibilities.get(rowIndex).isUsable();
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            Accessibility access = accessibilities.get(rowIndex);
            if(columnIndex == 0) {
                access.setEnabled((Boolean)aValue);
            }
        }

    }    
    
    public class Aggregator {
        private HashMap2D<String, String, Double> table;

        public Aggregator(int version) {
            List<String> levels = new ArrayList<>(MAX_LEVEL*MAX_LEVEL);
            if(version == 2) {
                for(int i = 1; i <= MAX_LEVEL; i++) {
                    levels.add("F" + i);
                }
                table = new HashMap2D<>(levels, Arrays.asList(
                        Type.Shop.toString(), Type.Green.toString()), 1.0/MAX_LEVEL);
            } else {
                for(int i = 1; i <= MAX_LEVEL; i++) {
                    for (int j = 1; j <= MAX_LEVEL; j++) {
                        levels.add("L" + i + "-F" + j);
                    }
                }
                table = new HashMap2D<>(levels, Arrays.asList(
                        Type.Shop.toString(), Type.Green.toString(), Type.Morphology.toString(), "Aggreg"), 1.0/MAX_LEVEL);
            }
        }
        
        public double getCoef(int elemLevel, int amLevel, Type type) {
            return table.getValue("L" + elemLevel + "-F" + amLevel, type.toString());
        }
        
        public double getCoef(int amLevel, Type type) {
            return table.getValue("F" + amLevel, type.toString());
        }
        
        public double getCoefAggreg(int elemLevel, int amLevel) {
            return table.getValue("L" + elemLevel + "-F" + amLevel, "Aggreg");
        }

        public HashMap2D<String, String, Double> getTable() {
            return table;
        }

        public void setTable(HashMap2D<String, String, Double> table) {
            this.table = table;
        }
        
        
    }
}
