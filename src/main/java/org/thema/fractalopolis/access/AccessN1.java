/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis.access;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import java.util.HashMap;
import java.util.Map;
import org.thema.common.fuzzy.DiscreteFunction;
import org.thema.common.fuzzy.MembershipFunction;
import org.thema.data.feature.Feature;
import org.thema.fractalopolis.Project;
import org.thema.fractalopolis.ifs.FractalElem;

/**
 *
 * @author Gilles Vuidel
 */
public class AccessN1 extends AbstractAccessibility {

    private MembershipFunction distance = new DiscreteFunction(new double[]{20000.0, 40000.0}, new double []{1.0, 0.0});
    
    public AccessN1(Project project) {
        super(project);
    }
    
    @Override
    public String getName() {
        return "Shop freq 1";
    }

    @Override
    public String getShortName() {
        return "shop1";
    }

    @Override
    public double calcAccess(FractalElem elem, TypeZone typeZone) {
        if(typeZone == TypeZone.Rural) {
            return 0.0;
        }
        double x = 0, y = 0;
        int nb = 0;
        for(Feature f : getProject().getCoverageLevel(Project.Layers.SHOP.toString(), 1).getFeatures()) {
            x += f.getGeometry().getCoordinate().x;
            y += f.getGeometry().getCoordinate().y;
            nb++;
        }
        if(nb == 0) {
            return 0;
        }
        
        x /= nb;
        y /= nb;
        OriginDistance origDistance = getProject().getFineAccess().getOriginDistance(elem.getGeometry().getCentroid(), Double.NaN);
        double dist = origDistance.getDistance(new GeometryFactory().createPoint(new Coordinate(x, y)));
        return distance.getValue(dist);
    }
    
    @Override
    public boolean isUsable() {
        return getProject().isLayerExist(Project.Layers.SHOP.toString());
    }
    
    @Override
    public int getFreq() {
        return 1;
    }
    
    @Override
    public Type getType() {
        return Type.Shop;
    }    
    
    @Override
    public Map<String, Object> getParams() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("Distance function", distance);
        return map;
    }

    @Override
    public void setParams(Map<String, Object> params) {
        distance = (DiscreteFunction) params.get("Distance function");
    }
}
