/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis.access;

import org.thema.fractalopolis.Project;
import org.thema.fractalopolis.Project.Layers;

/**
 *
 * @author gvuidel
 */
public class AccessMacroLeisure extends AbstractAccessMacro {

    public AccessMacroLeisure(Project project, int freq) {
        super(project, freq);
    }

    @Override
    public String getName() {
        return "Leisure freq " + getFreq();
    }

    @Override
    public String getShortName() {
        return "leisure" + getFreq();
    }
   
    @Override
    public Type getType() {
        return Type.Green;
    }    

    @Override
    protected Layers getLayer() {
        return Layers.LEISURE;
    }
}
