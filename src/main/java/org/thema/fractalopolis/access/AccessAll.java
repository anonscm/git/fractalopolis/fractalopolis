/*
 * Copyright (C) 2020 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis.access;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Point;
import org.thema.common.fuzzy.DiscreteFunction;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.DefaultFeatureCoverage;
import org.thema.data.feature.Feature;
import org.thema.fractalopolis.Project;
import org.thema.fractalopolis.Project.Layers;
import org.thema.fractalopolis.ifs.Fractal.Scale;
import org.thema.fractalopolis.ifs.FractalElem;

/**
 *
 * @author gvuidel
 */
public class AccessAll extends AbstractAccessibility {

    public static class TypeParam {
        public DiscreteFunction distance;
        public double potential;

        public TypeParam(DiscreteFunction distance) {
            this.distance = distance;
            potential = 1;
        }
        
        @Override
        public String toString() {
            return ((int)(potential*100)) + "% - " + distance.toString();
        }
    }
    
    private int freq;
    private Type type;
    private Scale scale;
    private DiscreteFunction defaultDistance;
    private Map<String, TypeParam> params;

    public AccessAll(Project project, Scale scale, Type type, int freq, DiscreteFunction defaultDistance) {
        super(project);
        this.scale = scale;
        this.type = type;
        this.freq = freq;
        this.defaultDistance = defaultDistance;
    }

    @Override
    public double calcAccess(FractalElem elem, TypeZone typeZone) {
        Geometry elemGeom = typeZone == TypeZone.Urban ? elem.getGeometry() : elem.getRuralGeometry();
        getParams(); // to initialize params if null
        double maxDist = 0;
        HashMap<String, Double> evalTypes = new HashMap<>(params.size());
        for(String type : params.keySet()) {
            evalTypes.put(type, 0.0);
            maxDist = Math.max(maxDist, params.get(type).distance.getPoints().lastKey());
        }
        double totPop = 0;
        DefaultFeatureCoverage<DefaultFeature> amenities = getProject().getCoverageLevel(getLayer().toString(), freq);
        for(Feature build : getProject().getCoverage(Layers.BUILD.toString()).getFeaturesIn(elemGeom)) {
            HashMap<String, Double> dists = new HashMap<>(params.size());
            for(String type : params.keySet()) {
                dists.put(type, Double.MAX_VALUE);
            }
            OriginDistance dist = getAccess().getOriginDistance(build.getGeometry().getCentroid(), maxDist);
            Envelope env = new Envelope(build.getGeometry().getCentroid().getCoordinate());
            env.expandBy(maxDist);
            for(Feature f : amenities.getFeatures(env)) {
                String type = f.getAttribute(Facility.TYPE_FIELD).toString();
                double d = dist.getDistance((Point)f.getGeometry());
                if(d < dists.get(type)) {
                    dists.put(type, d);
                }
            }
            double pop = ((Number)build.getAttribute(Project.HOUSING_ATTR)).doubleValue();
            for(String type : params.keySet()) {
                double evalDist = params.get(type).distance.getValue(dists.get(type));
                if(evalTypes.get(type) == null) {
                    System.out.println("boooo : " + type);
                }
                evalTypes.put(type, evalTypes.get(type) + evalDist*pop);
            }
            totPop += pop;
        }
        
        double eval = 0;
        double sumPot = 0;
        for(String type : params.keySet()) {
            eval += evalTypes.get(type) / totPop * params.get(type).potential;
            sumPot += params.get(type).potential;
        }
        return eval / sumPot;
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public String getName() {
        return scale.toString() + "_" + type.toString() + "_F" + freq;
    }

    @Override
    public String getShortName() {
        return getName();
    }
    
    @Override
    public boolean isUsable() {
        return getProject().isLayerExist(getLayer().toString());
    }
    
    @Override
    public int getFreq() {
        return freq;
    }

    private Layers getLayer() {
        return type == Type.Shop ? Layers.SHOP : Layers.LEISURE;
    }
    
    private Accessibilities getAccess() {
        return scale == Scale.MACRO ? getProject().getLargeAccess() : getProject().getFineAccess();
    }

    @Override
    public synchronized Map<String, Object> getParams() {
        if(params == null) {
            params = new TreeMap<>();
        }
        
        HashSet<String> types = new HashSet<>();
        for(Feature f : getProject().getCoverageLevel(getLayer().toString(), freq).getFeatures()) {
            types.add(f.getAttribute(Facility.TYPE_FIELD).toString());
        }
        types.removeAll(params.keySet());
        for(String type : types) {
            params.put(type, new TypeParam(defaultDistance));
        }
        
        HashMap<String, Object> map = new HashMap<>();
        map.putAll(params);
        return map;
    }

    @Override
    public void setParams(Map<String, Object> params) {
        this.params.clear();
        this.params.putAll((Map)params);
    }
}
