/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis.access;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import org.thema.common.fuzzy.DiscreteFunction;
import org.thema.common.fuzzy.MembershipFunction;
import org.thema.data.feature.Feature;
import org.thema.fractalopolis.Project;
import org.thema.fractalopolis.Project.Layers;
import org.thema.fractalopolis.ifs.FractalElem;

/**
 *
 * @author gvuidel
 */
public abstract class AbstractAccessMacro extends AbstractAccessibility {

    
    private int freq;
    
    private int maxStep = 2;
    private MembershipFunction diversity = new DiscreteFunction(new double[]{0.0, 4.0}, new double []{0.0, 1.0});

    public AbstractAccessMacro(Project project, int freq) {
        super(project);
        this.freq = freq;
        this.maxStep = freq-1;
    }

    @Override
    public double calcAccess(FractalElem elem, TypeZone typeZone) {
        if(typeZone == TypeZone.Rural) {
            return 0.0;
        }
        if(elem.getStep() <= maxStep) {
            HashSet types = new HashSet();
            for(Feature f : getProject().getCoverageLevel(getLayer().toString(), freq).getFeaturesIn(elem.getGeometry())) {
                types.add(f.getAttribute(Facility.TYPE_FIELD));
            }
            return diversity.getValue(types.size());
        } else {
            return calcAccess(elem.getParent(), typeZone);
        }
    }
    
    @Override
    public boolean isUsable() {
        return getProject().isLayerExist(getLayer().toString());
    }
    
    @Override
    public int getFreq() {
        return freq;
    }

    protected abstract Layers getLayer();

    protected int getMaxStep() {
        return maxStep;
    }

    protected MembershipFunction getDiversity() {
        return diversity;
    }
    
    @Override
    public Map<String, Object> getParams() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("Diversity function", diversity);
        map.put("Maximum step", maxStep);
        return map;
    }

    @Override
    public void setParams(Map<String, Object> params) {
        diversity = (MembershipFunction) params.get("Diversity function");
        maxStep = (int) params.get("Maximum step");
    }
}
