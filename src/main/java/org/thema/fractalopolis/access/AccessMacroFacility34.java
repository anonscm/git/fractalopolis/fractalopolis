/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis.access;

import java.util.HashSet;
import java.util.Map;
import org.thema.common.fuzzy.DiscreteFunction;
import org.thema.common.fuzzy.MembershipFunction;
import org.thema.data.feature.Feature;
import org.thema.fractalopolis.Project;
import org.thema.fractalopolis.Project.Layers;
import org.thema.fractalopolis.ifs.FractalElem;

/**
 *
 * @author gvuidel
 */
public class AccessMacroFacility34 extends AbstractAccessMacro {

    private MembershipFunction count = new DiscreteFunction(new double[]{0.0, 4.0}, new double []{0.0, 1.0});

    public AccessMacroFacility34(Project project, int freq) {
        super(project, freq);
    }

    @Override
    public String getName() {
        return "Shop freq " + getFreq();
    }

    @Override
    public String getShortName() {
        return "shop" + getFreq();
    }

    @Override
    public double calcAccess(FractalElem elem, TypeZone typeZone) {
        if(typeZone == TypeZone.Rural) {
            return 0.0;
        }
        if(elem.getStep() <= getMaxStep()) {
            HashSet types = new HashSet();
            int nb = 0;
            for(Feature f : getProject().getCoverageLevel(getLayer().toString(), getFreq()).getFeaturesIn(elem.getGeometry())){
                types.add(f.getAttribute(Facility.TYPE_FIELD));
                nb++;
            }
            return count.getValue(nb)*getDiversity().getValue(types.size());
        } else {
            return calcAccess(elem.getParent(), typeZone);
        } 
    }

    @Override
    protected Layers getLayer() {
        return Layers.SHOP;
    }
    
    @Override
    public Type getType() {
        return Type.Shop;
    }
    
    @Override
    public Map<String, Object> getParams() {
        Map<String, Object> map = super.getParams();
        map.put("Count function", count);
        return map;
    }

    @Override
    public void setParams(Map<String, Object> params) {
        super.setParams(params);
        count = (MembershipFunction) params.get("Count function");
    }
}
