/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis.access;

import org.locationtech.jts.geom.Point;
import org.thema.graph.SpatialGraph;
import org.thema.graph.pathfinder.DijkstraPathFinder;
import org.thema.graph.pathfinder.EdgeWeighter;

/**
 *
 * @author gvuidel
 */
public class NetworkDistance implements OriginDistance {

    private DijkstraPathFinder pathfinder;
    private SpatialGraph graph;
    
    public NetworkDistance(SpatialGraph graph, EdgeWeighter weighter, Point origin, double maxCost) {
        this.graph = graph;
        pathfinder = graph.getPathFinder(origin, weighter, maxCost);
    }
    
    @Override
    public double getDistance(Point dest) {
        Double cost = graph.getCost(pathfinder, dest);
        if(cost == null) {
            return Double.MAX_VALUE;
        } else {
            return cost;
        }
    }
    
}
