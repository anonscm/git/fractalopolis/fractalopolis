/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis.access;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.thema.common.fuzzy.DiscreteFunction;
import org.thema.data.feature.Feature;
import org.thema.fractalopolis.Project;
import org.thema.fractalopolis.ifs.FractalElem;

/**
 *
 * @author gvuidel
 */
public class AccessMicroLeisure extends AbstractAccessibility {

    private int freq;
    
    private DiscreteFunction distance = new DiscreteFunction(new double[]{60000.0, 100000.0}, new double []{1.0, 0.0});

    public AccessMicroLeisure(Project project, int freq) {
        super(project);
        this.freq = freq;
    }
    
    public AccessMicroLeisure(Project project, int freq, double[] dist, double[] eval) {
        this(project, freq);
        distance = new DiscreteFunction(Arrays.copyOf(dist, dist.length), Arrays.copyOf(eval, eval.length));
    }

    @Override
    public String getName() {
        return "Leisure freq " + freq;
    }

    @Override
    public String getShortName() {
        return "leisure" + freq;
    }

    @Override
    public double calcAccess(FractalElem elem, TypeZone typeZone) {
        if(typeZone == TypeZone.Rural) {
            return 0.0;
        }
        Double distMax = distance.getPoints().lastKey();
        OriginDistance origDistance = getProject().getFineAccess().getOriginDistance(elem.getGeometry().getCentroid(), distMax);
        HashMap<String, Double> distances = new HashMap<>();
        for(Feature f : getProject().getCoverageLevel(Project.Layers.LEISURE.toString(), freq).getFeatures()) {
            String type = (String) f.getAttribute(Facility.TYPE_FIELD);
            double dist = origDistance.getDistance(f.getGeometry().getCentroid());
            if(!distances.containsKey(type) || dist < distances.get(type)) {
                distances.put(type, dist);
            }
        }
        if(distances.isEmpty()) {
            return 0;
        }
        
        double eval = 0;
        for(Double dist : distances.values()) {
            eval += distance.getValue(dist);
        }
        return eval / distances.size();
    }
    
    @Override
    public boolean isUsable() {
        return getProject().isLayerExist(Project.Layers.LEISURE.toString());
    }
    
    @Override
    public int getFreq() {
        return freq;
    }
    
    @Override
    public Type getType() {
        return Type.Green;
    }  
    
    @Override
    public Map<String, Object> getParams() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("Distance function", distance);
        return map;
    }

    @Override
    public void setParams(Map<String, Object> params) {
        distance = (DiscreteFunction) params.get("Distance function");
    }
}
