/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fractalopolis.access;

import org.locationtech.jts.geom.Envelope;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.thema.common.fuzzy.DiscreteFunction;
import org.thema.common.fuzzy.MembershipFunction;
import org.thema.data.feature.DefaultFeatureCoverage;
import org.thema.fractalopolis.Project;
import org.thema.fractalopolis.Project.Layers;
import org.thema.fractalopolis.ifs.FractalElem;

/**
 *
 * @author gvuidel
 */
public class AccessN34 extends AbstractAccessibility {

    private double distCluster = 800;
    private MembershipFunction diversity = new DiscreteFunction(new double[]{0.0, 1.0, 2.0, 3.0}, new double []{0.01, 0.125, 0.25, 1.0});
    private MembershipFunction count = new DiscreteFunction(new double[]{0.0, 10.0}, new double []{0.01, 1.0});
    private DiscreteFunction distance = new DiscreteFunction(new double[]{3000.0, 10000.0}, new double []{1.0, 0.0});

    private int freq;
    
    private transient DefaultFeatureCoverage<ClusterFacility> clusterCov;
    
    public AccessN34(Project project, int freq) {
        super(project);
        this.freq = freq;
    }

    private synchronized DefaultFeatureCoverage<ClusterFacility> getClusterCov() {
        if(clusterCov == null) {
            clusterCov = new DefaultFeatureCoverage<>(
                    ClusterFacility.createClusters(
                            Facility.importFacilities(
                                    getProject().getCoverageLevel(Layers.SHOP.toString(), freq).getFeatures()).values(), distCluster/2));
        }
            
        return clusterCov;
    }
        
    @Override
    public String getName() {
        return "Shop freq " + freq;
    }
    @Override
    public String getShortName() {
        return "shop" + freq;
    }

    @Override
    public double calcAccess(FractalElem elem, TypeZone typeZone) {
        if(typeZone == TypeZone.Rural) {
            return 0.0;
        }
        Double distMax = distance.getPoints().lastKey();
        OriginDistance origDistance = getProject().getFineAccess().getOriginDistance(elem.getGeometry().getCentroid(), distMax);
        Envelope env = new Envelope(elem.getGeometry().getCentroid().getEnvelopeInternal());
        env.expandBy(distMax);
        List<ClusterFacility> features = getClusterCov().getFeatures(env);
        double phi = 1;
        for(ClusterFacility clust : features) {
            double d = distance.getValue(origDistance.getDistance(clust.getFacilityGeometry().getCentroid()));
            double n = count.getValue(clust.getNbFacilities());
            double delta = diversity.getValue(clust.getNbTypeFacilities());
            double attract = Math.pow(Math.pow(n, 1-delta)*d, 1-d) * Math.pow(1-(1-Math.pow(n, 1-delta))*(1-d), d);
            phi *= 1-attract;
        }
        return 1 - phi;
    }
    
    @Override
    public boolean isUsable() {
        return getProject().isLayerExist(Layers.SHOP.toString());
    }
    
    @Override
    public int getFreq() {
        return freq;
    }
    
    @Override
    public Type getType() {
        return Type.Shop;
    }    
    
    @Override
    public Map<String, Object> getParams() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("Diversity function", diversity);
        map.put("Count function", count);
        map.put("Distance function", distance);
        map.put("Cluster distance", distCluster);
        return map;
    }

    @Override
    public void setParams(Map<String, Object> params) {
        diversity = (MembershipFunction) params.get("Diversity function");
        distance = (DiscreteFunction) params.get("Distance function");
        count = (MembershipFunction) params.get("Count function");
        distCluster = (Double) params.get("Cluster distance");
    }
}
